import logging

from cor3python.core_objects.api.api_status import ApiStatus



def log_api_error(api_status: ApiStatus):
    """Log to stderr, information about a API with failed status

    Args:
        api_status (ApiStatus): [description]
    """

    # dont log anything if its a pass
    if api_status.status:
        return

    if len(api_status.description) > 0:
        logging.error("[%s] - %s - %s", api_status.core_status, api_status.description, api_status.request.message)
    else:
        logging.error("[%s] - %s", api_status.core_status, api_status.request.message)
