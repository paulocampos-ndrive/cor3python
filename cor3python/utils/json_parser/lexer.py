from .constants import *

JSON_QUOTE = '"'
JSON_WHITESPACE = [' ', '\t', '\b', '\n', '\r']
JSON_SYNTAX = [JSON_COMMA, JSON_COLON, JSON_LEFTBRACKET, JSON_RIGHTBRACKET,
               JSON_LEFTBRACE, JSON_RIGHTBRACE]

FALSE_LEN = len('false')
TRUE_LEN = len('true')
NULL_LEN = len('null')

NUMBER_CHARS = [str(d) for d in range(0, 10)] + ['-', '.']


def lex_identifier(string):
    json_string = ''

    if not string[0].isalpha():
        return None, string

    for c in string:
        if c in [JSON_QUOTE, JSON_COMMA, JSON_SYNTAX, JSON_COLON, JSON_RIGHTBRACE, JSON_RIGHTBRACKET]:
            return json_string, string[len(json_string):]
        else:
            json_string += c

    if len(json_string) == len(string):
        return json_string, ""

    raise Exception('Expected end-of-identifier')

def lex_string(a_string):
    if a_string is None or a_string == "" or a_string[0] != JSON_QUOTE:
        return None, a_string

    j = 1
    i = 1
    escape = False
    ret_string = []

    while True:
        if a_string[i] == '\"' and not escape:
            break

        if not escape and a_string[i] == '\\':
            escape = True
            # copy previous chars
            ret_string.append(a_string[j:i])
            next_char = a_string[i + 1]
            if next_char == 'n':
                ret_string.append('\n')
                i += 1
            j = i + 1
        else:
            escape = False
        i += 1

    # copy remaining chars and return
    ret_string.append(a_string[j:i])
    return "".join(ret_string), a_string[i + 1:]

def lex_number(string):
    json_number = ''

    for c in string:
        if c in NUMBER_CHARS:
            json_number += c
        else:
            break

    rest = string[len(json_number):]

    if not len(json_number):
        return None, string

    if '.' in json_number:
        return float(json_number), rest

    return int(json_number), rest

def lex_bool(string):
    string_len = len(string)

    if string_len >= TRUE_LEN and \
         string[:TRUE_LEN] == 'true':
        return True, string[TRUE_LEN:]
    if string_len >= FALSE_LEN and \
         string[:FALSE_LEN] == 'false':
        return False, string[FALSE_LEN:]

    return None, string


def lex_null(string):
    string_len = len(string)

    if string_len >= NULL_LEN and \
         string[:NULL_LEN] == 'null':
        return True, string[NULL_LEN:]

    return None, string


def lex(string):
    tokens = []

    while len(string):
        json_string, string = lex_string(string)
        if json_string is not None:
            tokens.append(json_string)
            continue

        json_number, string = lex_number(string)
        if json_number is not None:
            tokens.append(json_number)
            continue

        json_bool, string = lex_bool(string)
        if json_bool is not None:
            tokens.append(json_bool)
            continue

        json_id, string = lex_identifier(string)
        if json_id is not None:
            tokens.append(json_id)
            continue

        json_null, string = lex_null(string)
        if json_null is not None:
            tokens.append(None)
            continue

        c = string[0]

        if c in JSON_WHITESPACE:
            # Ignore whitespace
            string = string[1:]
        elif c in JSON_SYNTAX:
            tokens.append(c)
            string = string[1:]
        else:
            raise Exception('Unexpected character: {}'.format(c))

    return tokens
