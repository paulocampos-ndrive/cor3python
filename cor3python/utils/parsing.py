

def str2int(v):
    if v is None or v == "":
        return v
    return int(v)


def str2float(s):
    if s is None or s == 'None' or s == '':
        return None
    return float(s)


def str2bool(v):
    if v is None:
        return False
    if type(v) == bool:
        return v
    return v.lower() in ("yes", "true", "t", "1")


def bool2str(v):
    if v:
        return "yes"
    else:
        return "no"


def escape(a_string: str) -> str:
    s1 = a_string
    try:
        special_chars = ['\\', '\"', '\n']
        replace_chars = ["\\\\", "\\\"", "\\n"]
        for i, char in enumerate(special_chars):
            s1 = s1.replace(char, replace_chars[i])
        return s1
    except:
        return s1        