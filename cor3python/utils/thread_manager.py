from threading import Thread, Lock
import queue
import logging
from typing import Callable


class WorkerThread(object):
    """ Thread with work queue """

    def __init__(self, a_thread_id:str):
        self.name = a_thread_id
        self._thread = Thread(name=a_thread_id, target=self._run)
        self._queue = queue.Queue()
        self._running = False

    def is_running(self):
        return self._running

    def start(self):
        self._running = True
        self._thread.start()
        logging.info("Thread %s Started", self.name)

    def stop(self):
        if self.is_running():
            self._queue.put(None)
            self._running = False
            self._queue.join()

    def run(self, a_work, *args, a_callback= None, **kwargs):
        """ Add new work to the queue, a callback is accepted """
        def work():
            a_work(*args, **kwargs)

        self._queue.put(work)
        if a_callback:
            self._queue.put(a_callback)

    def _run(self):
        while self._running:
            work = self._queue.get()
            if work is None:
                self._queue.task_done()
                self._running = False
            else:
                try:
                    work()
                except Exception as err:
                    logging.error("%s", err)
                finally:
                    self._queue.task_done()
        logging.info("finished")


class WorkerThreadSingle(object):
    """ Thread object for single execution, allows a callback after execution """

    def __init__(self, a_thread_id: str, a_work: Callable[...,None], a_callback: Callable[...,None]= None):
        self.name = a_thread_id
        self._work = a_work
        self._cb = a_callback
        self._thread = Thread(name=a_thread_id, target=self._run)
        self._running = False

    def __repr__(self):
        return self.name

    def is_running(self):
        return self._running

    def start(self):
        self._running = True
        self._thread.start()

    def stop(self):
        self._running = False
        self._thread.join(timeout=5)
        if self._thread.is_alive():
            logging.error("failed to join")

    def _run(self):
        try:
            self._work()
        except Exception as err:
            logging.error("%s", err)
        try:
            if self._cb:
                self._cb()
        except Exception as err:
            logging.error("%s", err)
        logging.info("finished")


class WorkThreadManager(object):
    """ Manages all running Threads """

    def __init__(self):
        self._worker_list = list()
        self._lock_thread_list = Lock()
        self._thread_name_count = 0

    def _worker_exists(self, a_thread_id: str):
        for worker in self._worker_list:
            if worker.name == a_thread_id:
                return worker
        return None

    def get_thread(self, a_thread_id:str = None) -> WorkerThread:
        """ Used to return a thread (creates if not exists), already started"""

        if a_thread_id:
            thread_name = a_thread_id
        else:
            # generate a random name
            self._thread_name_count += 1
            thread_name = "THREAD_{}".format(self._thread_name_count)

        self._lock_thread_list.acquire()
        worker_ = self._worker_exists(thread_name)
        if worker_ is not None:
            self._lock_thread_list.release()
            return worker_

        worker = WorkerThread(thread_name)
        self._worker_list.append(worker)
        worker.start()
        self._lock_thread_list.release()
        return worker

    def get_thread_single(self, work: Callable[...,None], a_thread_id:str = None, a_callback: Callable[...,None]= None) -> WorkerThreadSingle:
        """ Returns a new thread (not started) for async execution of a_work
            Thread is not managed """
        if a_thread_id:
            thread_name = a_thread_id
        else:
            # generate a random name
            self._thread_name_count += 1
            thread_name = "THREAD_{}".format(self._thread_name_count)            

        worker = WorkerThreadSingle(thread_name, work, a_callback=a_callback)
        self._lock_thread_list.acquire()
        self._worker_list.append(worker)
        self._lock_thread_list.release()
        return worker

    def remove(self, a_thread: WorkerThread):
        """ Stop and remove thread from thread pool"""

        self._lock_thread_list.acquire()
        a_thread.stop()
        self._worker_list.remove(a_thread)
        self._lock_thread_list.release()

    def stop(self):
        """ Stops and remove all threads from thread pool"""

        self._lock_thread_list.acquire()
        workers_list = self._worker_list
        for worker in workers_list:
            worker.stop()
        self._lock_thread_list.release()
        logging.info("All threads finished!")

# Singleton for thread manager
thread_manager = WorkThreadManager()
