from enum import IntEnum
import ctypes
import logging
import os
import sys

from cor3python.utils.thread_manager import thread_manager
from cor3python.messaging.native_dispatcher import NativeDispatcher
from cor3python.messaging.socket_dispatcher import SocketDispatcher
from cor3python.core_objects.search import Search
from cor3python.core_objects.licensing import Licensing
from cor3python.core_objects.environment import Environment
from cor3python.core_objects.routing import Routing
from cor3python.core_objects.data import Data
from cor3python.core_objects.vector_tiles import VectorTiles
from cor3python.core_objects.traffic_client import TrafficClient



class Core(object):
    """ Main class for all cor3 objects """

    class Cor3InitParams(ctypes.Structure):
        response_handler_fp = ctypes.CFUNCTYPE(None, ctypes.c_void_p, ctypes.c_size_t)
        graphics_task_fp = ctypes.CFUNCTYPE(None)

        _fields_ = [("response_handler", response_handler_fp),
                    ("debug_token_list", ctypes.c_char_p),
                    ("debug_folder", ctypes.c_char_p),
                    ("memory_threshold", ctypes.c_uint32),
                    ("max_memory_threshold", ctypes.c_uint32),
                    ("fd_limit", ctypes.c_uint32),
                    ("worker_thread_count", ctypes.c_uint32),
                    ("listen_port", ctypes.c_uint16),
                    ("uses_custom_mm", ctypes.c_bool),
                    ("server_mode", ctypes.c_bool),
                    ("cache_limit_routing", ctypes.c_uint32),
                    ("cache_limit_not_routing", ctypes.c_uint32),
                    ("graphics_task_req_callback", graphics_task_fp),
                    ("graphics_engine", ctypes.c_uint8)]

        class RenderingEngine(IntEnum):
            kNone = 0
            kOpenGLES20 = 1
            kMetal = 2


    class CommMode(IntEnum):
        """ IntEnum for existing communication modes"""

        kNative = 0
        kSocket = 1


    def __init__(self, a_verbose: bool = False):

        # messaging instance for comms
        self.messaging = None

        # working mode: native vs socket
        self._mode = None

        # libcor3 instance in native mode
        self._lib_cor3 = None

        # Should bw using graphics? Map?
        self._use_graphics = False

        # logging setup
        logging.root.setLevel(logging.getLevelName('INFO'))

        # cor3 configs
        self._cor3_params = None
        self.cor3_verbose = a_verbose
        self.cor3_debug_tokens = ""
        self.cor3_safe_memory = 1024
        self.cor3_max_memory = 2048
        self.cor3_fd_limit = 127
        self.cor3_port = 0
        self.cor3_uses_custom_mm = False
        self.cor3_server_mode = False
        self.cache_limit_routing = 0
        self.cache_limit_not_routing = 0
        self.worker_thread_count = 2

        self._core_thread = thread_manager.get_thread("CORE")
        
        # base core objects
        self.search: Search = None
        self.routing: Routing = None
        self.licensing: Licensing = None
        self.traffic_client: TrafficClient = None
        self.data: Data = None
        self.environment: Environment = None
        self.vector_tiles: VectorTiles = None

    def _cb_graphics_update(self):
        """ Callback called from cor3 for background update without rendering """

        self._lib_cor3.Cor3UpdateGraphicsWithoutRendering()

    def start_native(self,  a_lib_cor3:str, a_graphics_mode: Cor3InitParams.RenderingEngine = Cor3InitParams.RenderingEngine.kNone, a_use_graphics:bool = False) -> bool:
        """Start core, using native 'c' API

        Args:
            a_lib_cor3 (str): full path for library
            a_graphics_mode (Cor3InitParams.RenderingEngine): Graphics engine

        Returns:
            bool: success/failure
        """
        self._use_graphics = a_use_graphics
        self._mode = Core.CommMode.kNative

        try:
            self._lib_cor3 = ctypes.CDLL(os.path.expanduser(a_lib_cor3))
        except Exception as err:
            logging.error("LibCor3 Error - {}".format(err))
            thread_manager.stop()
            sys.exit(-1)

        logging.info("COR3 Loaded")
        self._lib_cor3.OsalInitialize()
        logging.info("OsalInitialize")

        self.messaging = NativeDispatcher(self._lib_cor3, self.cor3_verbose)

        self._cor3_params = Core.Cor3InitParams()
        self._cor3_params.graphics_task_req_callback = Core.Cor3InitParams.graphics_task_fp(self._cb_graphics_update)
        self._cor3_params.response_handler = Core.Cor3InitParams.response_handler_fp(self.messaging.response_handler)
        self._cor3_params.debug_token_list = self.cor3_debug_tokens.encode('utf-8')
        self._cor3_params.memory_threshold = self.cor3_safe_memory
        self._cor3_params.max_memory_threshold = self.cor3_max_memory
        self._cor3_params.fd_limit = self.cor3_fd_limit
        self._cor3_params.listen_port = self.cor3_port
        self._cor3_params.uses_custom_mm = self.cor3_uses_custom_mm
        self._cor3_params.cache_limit_routing = self.cache_limit_routing
        self._cor3_params.cache_limit_not_routing = self.cache_limit_not_routing
        self._cor3_params.worker_thread_count = self.worker_thread_count
        self._cor3_params.server_mode = self.cor3_server_mode
        self._cor3_params.graphics_engine = a_graphics_mode

        ret = self._lib_cor3.Cor3Initialize(ctypes.byref(self._cor3_params))
        if ret is None or ret != 1:
            logging.error("Error in Cor3Initialize")
            return False
        logging.info("Cor3Initialize")

        # Start messaging send/recv
        self.messaging.start()

        # Start base objects
        self._start_default_objects()
        return True

    def stop(self) -> bool:
        self.messaging.stop()
        thread_manager.stop()
        if self._mode == Core.CommMode.kNative:
            self._lib_cor3.Cor3Finalize()
            self._lib_cor3.OsalFinalize()

        handle = self._lib_cor3._handle
        dlclose_func = ctypes.cdll.LoadLibrary(None).dlclose
        dlclose_func.argtypes = [ctypes.c_void_p]
        del self._lib_cor3
        dlclose_func(handle)
        
        self._lib_cor3 = None
        self._cor3_params = None
        self.messaging = None
        return True

    def _start_default_objects(self):
        """Start default cor3 objects
        """
        self.search = Search(self.messaging)
        self.licensing = Licensing(self.messaging, a_thread=self._core_thread)
        self.environment = Environment(self.messaging, a_thread=self._core_thread)
        self.routing = Routing(self.messaging, a_thread=self._core_thread)
        self.traffic_client = TrafficClient(self.messaging, a_thread=self._core_thread)
        self.data = Data(self.messaging, a_thread=self._core_thread)
        self.vector_tiles = VectorTiles(self.messaging, a_thread=self._core_thread)
