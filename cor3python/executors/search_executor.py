from queue import Queue
from enum import IntEnum
import time
import logging
from functools import partial
from threading import Event

from cor3python.core_objects.common.types.point_2d import Point2D
from cor3python.core import Core
from cor3python.core_objects.search import Search
from cor3python.utils.thread_manager import thread_manager


class SearchTask:
    class TaskType(IntEnum):
        Get = 0
        Start = 1
        StartFullSearch = 2
        Geocode = 3
        ReverseGeocode = 4
        GetPoiDetails = 5
        PoiCategoriesResouces = 6
        StartNearbySearch = 7

    def __init__(
        self,
        a_task_type: TaskType,
        a_task_params: list,
        a_language_code="en",
        a_location: Point2D = None,
    ) -> None:
        super().__init__()

        self._type = a_task_type
        self._params = a_task_params
        self._results = None
        self._executed = Event()
        self._error: str = None
        self._locale = a_language_code
        self._location = a_location
        self._searcher: Search = None

    @property
    def searcher(self):
        return self._searcher

    @searcher.setter
    def searcher(self, a_searcher):
        self._searcher = a_searcher

    @property
    def results(self):
        return self._results

    @results.setter
    def results(self, a_results):
        self._results = a_results

    def task_type(self):
        return self._type

    def task_params(self):
        return self._params

    def set_executed(self):
        self._executed.set()

    def is_executed(self):
        return self._executed.is_set()

    def wait_for_execution(self, a_timeout: float = None):
        """Wait for task completion

        Args:
            a_timeout (float, optional): Max wait time in sec.
        """
        self._executed.wait(a_timeout)
        if a_timeout is not None and not self.is_executed():
            self._error = "TIMEOUT"
            self._searcher.stop()

    @property
    def locale(self):
        return self._locale

    @property
    def location(self):
        return self._location

    @property
    def error(self):
        return self._error

    @error.setter
    def error(self, a_error: str):
        self._error = a_error


class SearchExecutor:
    def __init__(
        self, a_core: Core, a_name: str = None, a_num_workers: int = 1
    ) -> None:
        super().__init__()

        if a_name:
            self.name = a_name
        else:
            self.name = "SEARCH_EXECUTOR_{}".format(int(time.time()))

        self._num_workers = a_num_workers
        self._core = a_core
        self._searcher_list = list()
        self._tasks_queue = Queue()
        self._threads = list()
        self._running = False

        # create searchers
        self._init_workers(self._num_workers)

    def __del__(self):
        for searcher in self._searcher_list:
            searcher.destroy()

    def add_task(self, a_task: SearchTask):
        self._tasks_queue.put(a_task)

    def run_sync(self):
        """Run all tasks in Queue, and wait for finish."""
        self._start()
        self._stop()

    def start_async(self):
        """Start Executing tasks in queue, and keeps processing more..."""
        self._start()

    def stop_async(self):
        """Finish running tasks, and wait for finish"""
        self._stop()

    def _init_workers(self, a_num_workers: int):
        for _ in range(a_num_workers):
            self._searcher_list.append(Search(self._core.messaging))

    def _run_task(self, a_task: SearchTask, a_searcher: Search):
        # Set task configuration
        try:
            a_searcher.locale = a_task.locale

            # stop any ongoing search
            # a_searcher.stop()

            if a_task.location:
                a_searcher.position = a_task.location

            a_task.searcher = a_searcher

            # Run specific API

            if a_task.task_type() == SearchTask.TaskType.Geocode:
                a_task.results = a_searcher.geocode(*a_task.task_params())

            if a_task.task_type() == SearchTask.TaskType.Start:
                a_task.results = a_searcher.start_search(*a_task.task_params())

            if a_task.task_type() == SearchTask.TaskType.StartFullSearch:
                a_task.results = a_searcher.start_full_search(*a_task.task_params())

            if a_task.task_type() == SearchTask.TaskType.StartNearbySearch:
                a_task.results = a_searcher.start_nearby_search(*a_task.task_params())

            if a_task.task_type() == SearchTask.TaskType.Get:
                a_task.results = a_searcher.get(*a_task.task_params())

            if a_task.task_type() == SearchTask.TaskType.ReverseGeocode:
                a_task.results = a_searcher.reverse_geocode(*a_task.task_params())

            if a_task.task_type() == SearchTask.TaskType.GetPoiDetails:
                a_task.results = a_searcher.get_poi_details(*a_task.task_params())

            if a_task.task_type() == SearchTask.TaskType.PoiCategoriesResouces:
                a_task.results = a_searcher.get_poi_categories_resources(*a_task.task_params())

        except Exception as err:
            a_task.error = str(err)

    def _worker_process(self, a_searcher: Search):
        # Loop until task in None
        for task in iter(self._tasks_queue.get, None):
            try:
                self._run_task(task, a_searcher)
            except Exception as err:
                logging.error("Search Task error: {}".format(err))
            finally:
                task.set_executed()

                # mark queue task as done
                self._tasks_queue.task_done()

        # Last None Task
        self._tasks_queue.task_done()

    def _start(self):
        if self._running:
            raise Exception("Search Executor [{}] - Already Running".format(self.name))

        self._running = True

        # start worker threads with its own searcher instance
        for i in range(self._num_workers):
            worker_thread = thread_manager.get_thread_single(
                partial(self._worker_process, self._searcher_list[i]),
                "{}_WORKER_{}".format(self.name, i),
            )
            self._threads.append(worker_thread)
            worker_thread.start()

    def _stop(self):
        # put stop tasks
        for _ in range(self._num_workers):
            self.add_task(None)
        for t in self._threads:
            t.stop()
        self._running = False


    @property
    def over_capacity(self) -> bool:
        if self._tasks_queue.qsize() > 1:
            return True
        return False