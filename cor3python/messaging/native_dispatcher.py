import logging
import ctypes
from functools import partial

from cor3python.utils.thread_manager import thread_manager
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.messaging.core_request import CoreRequest
from cor3python.messaging.core_response import CoreResponse


class NativeDispatcher(BaseDispatcher):
    def __init__(self, a_libcor3, a_verbose=False):

        super().__init__(a_verbose=a_verbose)

        self.libcor3 = a_libcor3
        self._buffer = []
        self._connected = False

        # Threads
        self._thread_recv = thread_manager.get_thread(BaseDispatcher.THREAD_RECV)

    def _connect(self):
        self._connected = True

    def _disconnect(self):
        self._connected = False

    def _send(self, a_request: CoreRequest):
        # call 'c' method with UTF-8 string
        self.libcor3.Cor3SendMessage(a_request.build().encode('utf-8') + b"\n")

    def response_handler(self, a_ptr: int, a_size: int):
        """ callback from cor3 with responses, delegate to receive thread """

        received = ctypes.string_at(a_ptr, a_size)
        self._thread_recv.run(partial(self._receive_worker, received))

    def _receive_worker(self, received: bytes):
        """ Thread that handles data coming from cor3 lib """

        # CHINESE WORKAROUND
        received = received.replace(b"\xE2\x80\x8B", b"")

        resp = received.split(b"\x0A")
        if len(resp) > 1:
            resp[0] = b"".join(self._buffer) + resp[0]
            self._buffer = []

        if received[-1] != b"\x0A":  # is last message not complete?
            self._buffer.append(resp.pop())
        else:
            self._buffer = []

        for r in resp:
            response = CoreResponse().load_raw(r.decode('utf-8'))
            try:
                self._process_response(response)

                # callback's
                if self._recv_callback:
                    self._recv_callback(response)
            except Exception as err:
                logging.error("%s - %s", response, err)
