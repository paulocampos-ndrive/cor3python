import re
from cor3python.utils.json_parser import from_string


class CoreResponse:

    class ResponseType:
        STATUS = 'S'
        DATA = 'D'

    def __init__(self):
        self._verbose = True
        self._id = None
        self._type = None
        self._status = None
        self._content = None
        self._raw_string = None

        # Data parsed from core message, does not include message type and status.
        self._data = {}

    def load_raw(self, a_raw_string:str):
        """Load basic information about response:
            - invoke_id
            - message type: S or D

        Args:
            a_raw_string (str): raw string from cor3

        Raises:
            Exception: Error parsing information

        """
        self._raw_string = a_raw_string

        if len(a_raw_string) == 0:
            raise Exception("Invalid Response! - {0}".format(a_raw_string))

        match = re.match(r"(\d+),(\w),(.+)$", a_raw_string)
        if match:
            (i, t, m) = (match.group(1), match.group(2), match.group(3))
            self._id = int(i)
            self._type = t

            if self._type == 'D':
                self._content = m
            else:
                match = re.match(r"(\w+)\s+(.*)$", m)
                if match:
                    (status, content) = (match.group(1), match.group(2))
                    self._status = status
                    self._content = content
                else:
                    self._content = ""
                    self._status = m

        else:
            raise Exception("Invalid Response! - {0}".format(a_raw_string))

        return self

    def parse(self):
        """Parses entire string into python objects: dict, list, int/float, str
        """
        self._data = {}
        self._data = from_string(self._content)
        return self._data

    @property
    def verbose(self):
        return self._verbose

    @verbose.setter
    def verbose(self, a_verbose):
        self._verbose = a_verbose

    @property
    def invoke_id(self):
        return self._id

    @property
    def content(self):
        return self._content

    @property
    def response_type(self):
        return self._type

    @property
    def core_status(self):
        return self._status

    def __str__(self):
        return self._raw_string

    @property
    def data(self):
        return self._data