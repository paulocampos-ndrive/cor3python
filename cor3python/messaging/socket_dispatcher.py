import socket
import logging
import select
import errno

from cor3python.utils.thread_manager import thread_manager
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.messaging.core_request import CoreRequest
from cor3python.messaging.core_response import CoreResponse


class SocketDispatcher(BaseDispatcher):
    def __init__(self,
                 a_ip: str,
                 a_port: int,
                 a_verbose: bool=False):

        super().__init__(a_verbose=a_verbose)
        self._connected = False
        self._socket = None

        self._thread_recv = None

        self.ip = a_ip
        self.port = a_port

    def _connect(self):
        try:
            self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._socket.settimeout(5)
            self._socket.connect((self.ip, int(self.port)))
            self._connected = True
            self._thread_recv = thread_manager.get_thread_single(self._receive_worker, BaseDispatcher.THREAD_RECV)
            self._thread_recv.start()
        except socket.error as e:
            logging.error("Socket error: %s", e)
            self._connected = False

        return self.connected

    def _disconnect(self):
        if not self._connected:
            return
        self._socket.shutdown(socket.SHUT_RDWR)
        self._socket.detach()
        self._socket = None
        self._connected = False

    def _send(self, a_request: CoreRequest):
        try:
            self._socket.sendall(a_request.build().encode('utf-8') + b"\n")
        except socket.error as e:
            logging.error("Socket send error: %s", e)

    def _receive_worker(self):

        buffer = b""
        while self._connected:
            try:

                try:
                    select.select([self._socket,], [self._socket,], [], 5)
                except select.error:
                    self._socket.shutdown(2)  # 0 = done receiving, 1 = done sending, 2 = both
                    if socket is not None:
                        self._socket.close()
                    # connection error event here, maybe reconnect
                    self.stop()
                    break

                try:
                    if self._socket is None:
                        continue
                    received = self._socket.recv(4096)
                except socket.error as e:
                    if e.errno != errno.ECONNRESET:
                        raise  # Not the error we are looking for
                    self.stop()
                    break

                # CHINESE WORKAROUND
                received = received.replace(b"\xE2\x80\x8B", b"")
                # socket disconnected on the other side
                if received is None or len(received) == 0:
                    self._connected = False
                    if self._socket is not None:
                        self._socket.close()
                    break
                data = buffer + received
                buffer = b""
                resp = data.split(b"\x0A")
                if data[-1] != b"\x0A":  # last char, of last response
                    if len(resp) > 1:
                        buffer = resp.pop()
                    else:
                        buffer = data
                        continue
                for r in resp:
                    r_as_string = r.decode('utf-8')
                    response = CoreResponse().load_raw(r_as_string)

                    try:
                        self._process_response(response)
                        if self._recv_callback:
                            self._recv_callback(response)
                    except Exception as err:
                        logging.error("%s - %s", response, err)

            except Exception as e:
                logging.error("%s", e)
                continue

        if self._socket is not None:
            self._socket.close()
