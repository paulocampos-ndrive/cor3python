from threading import Lock
from typing import Callable
from functools import partial
import logging

from cor3python.utils.thread_manager import thread_manager
from cor3python.messaging.core_request import CoreResponse, CoreRequest


class BaseDispatcher:
    THREAD_SEND = 'THREAD_SEND'
    THREAD_RECV = 'THREAD_RECV'

    def __init__(self, a_verbose: bool=False):
        self._verbose = a_verbose
        self._connected = False

        self._requests = dict()

        self._available_ids = []
        self._max_used_id = 0

        self._lock_ids = Lock()
        self._lock_requests = Lock()

        # Callbacks for send/receive actions
        self._send_callback = None
        self._recv_callback = None

        # Threads
        self._thread_send = thread_manager.get_thread(BaseDispatcher.THREAD_SEND)

    def set_callbacks(self, a_send: Callable[[CoreRequest], None]=None, a_recv: Callable[[CoreResponse], None]=None):
        self._send_callback = a_send
        self._recv_callback = a_recv

    def start(self):
        self._connect()

    def stop(self):
        if self._connected:
            self._disconnect()

    def _connect(self):
        raise Exception("Not implemented")

    def _disconnect(self):
        raise Exception("Not implemented")

    def _send_worker(self):
        raise Exception("Not implemented")

    def _receive_worker(self):
        raise Exception("Not implemented")

    def _send(self, a_request: CoreRequest):
        raise Exception("Not implemented")

    def _get_id(self) -> int:
        self._lock_ids.acquire(True)
        if len(self._available_ids) > 0:
            _id = self._available_ids.pop(0)
        else:
            self._max_used_id += 1
            _id = self._max_used_id
        self._lock_ids.release()
        return _id

    def _free_id(self, a_id: int):
        if a_id == 0:
            return
        self._lock_ids.acquire(True)
        self._available_ids.append(a_id)
        self._lock_ids.release()

    def _validate_invoke_id(self, a_id: int) -> bool:
        if a_id is None or (a_id in self._requests and a_id > 0):
            return False
        return True

    def send(self, a_request: CoreRequest):
        if not self._connected:
            raise Exception("Not connected")

        # attribute a valid invoke_id
        if not self._validate_invoke_id(a_request.invoke_id):
            a_request.invoke_id = self._get_id()

        # put request in current running requests list
        self._lock_requests.acquire()
        self._requests[a_request.invoke_id] = a_request
        self._lock_requests.release()
        self._thread_send.run(partial(self._send_worker, a_request))

    def _send_worker(self, a_request: CoreRequest):
        try:
            # build request
            a_request.build()

            if self._verbose and a_request.verbose:
                logging.info("%s", a_request)

            self._send(a_request)

            # callback's
            if self._send_callback:
                self._send_callback(a_request)

        except Exception as err:
            logging.error("%s", err)

    def _process_response(self, a_response: CoreResponse):
        # responsible for processing responses

        # if verbose, send to STDOUT
        if self._verbose:
            logging.info("%s", a_response)

        # discard invoke_ids 0
        if a_response.invoke_id == 0:
            return

        # check invoke_id
        if a_response.invoke_id not in self._requests:
            logging.error("Unknown Response - %s", a_response)
            return

        request = self._requests[a_response.invoke_id]
        a_response.verbose = request.verbose
        request.process_response(a_response)

        # if this response was a status, free invoke id and remove this request from the list
        if a_response.response_type == CoreResponse.ResponseType.STATUS:
            self._free_id(a_response.invoke_id)
            self._lock_requests.acquire()
            del self._requests[a_response.invoke_id]
            self._lock_requests.release()
