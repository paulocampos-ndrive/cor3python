from threading import Event
from functools import partial
import logging

from cor3python.utils.thread_manager import WorkerThread
from cor3python.messaging.core_response import CoreResponse


class CoreRequest:

    def __init__(self, a_processor_thread:WorkerThread = None, a_verbose=True):
        self._raw_string = None
        self._invoke_id = None
        self._message = None
        self._verbose = a_verbose

        self._processor_thread = a_processor_thread

        self._status_callbacks = list()
        self._data_callbacks = list()

        self._finished = Event()
        self._status = None
        self._status_content = None
        self._core_status = None
        self._data = {}

    @property
    def invoke_id(self) -> int:
        return self._invoke_id

    @invoke_id.setter
    def invoke_id(self, a_id:int):
        self._invoke_id = a_id

    @property
    def message(self) -> str:
        return self._message

    @message.setter
    def message(self, a_message: str):
        self._message = a_message

    @property
    def data(self):
        return self._data

    def set_thread(self, a_thread:WorkerThread ):
        self._processor_thread = a_thread

    @property
    def finished(self)-> bool:
        """Returns the status of the request.

        Returns:
            [bool]: True -> is finished, False -> Not finished
        """
        return self._finished.is_set()

    @finished.setter
    def finished(self, a_value: bool):
        """Set the request as finished or not

        Args:
            a_value (bool): True -> Finished, False -> Not finished
        """
        if a_value:
            self._finished.set()
        else:
            self._finished.clear()

    def wait(self, a_timeout:float = None):
        """Wait for the request conclusion. Status from cor3

        Args:
            a_timeout ([float], optional): Wait time in sec. Defaults to None = unlimited.
        """
        self._finished.wait(a_timeout)

    def register_status_callback(self, a_cb):
        if a_cb is not None:
            self._status_callbacks.append(a_cb)

    def register_data_callback(self, a_cb):
        if a_cb is not None:
            self._data_callbacks.append(a_cb)

    @property
    def verbose(self) -> bool:
        return self._verbose

    @verbose.setter
    def verbose(self, a_verbose: bool):
        self._verbose = a_verbose

    @property
    def status(self) -> str:
        return self._status

    @status.setter
    def status(self, a_status:str):
        self._status = a_status

    @property
    def status_content(self) -> str:
        return self._status_content

    @property
    def core_status(self) -> str:
        return self._core_status

    @status_content.setter
    def status_content(self, a_status:str):
        self._status_content = a_status.strip()

    def build(self) -> str:
        self._raw_string = "{0},{1}".format(self._invoke_id, self._message)
        return self._raw_string

    def __str__(self):
        return str(self._raw_string)

    def __repr__(self):
        return str(self._raw_string)

    def _run_status_callbacks(self):
        for callback in self._status_callbacks:
            try:
                if self._processor_thread:
                    # delegate to given worker
                    self._processor_thread.run(partial(callback, self))
                else:
                    # no worker, run on current thread
                    callback(self)
            except Exception as err:
                logging.error("Error on callback call: %s", err)
                logging.error("From request: %s", self)      

        # Mark request as finished
        if self._processor_thread:
            self._processor_thread.run(self._finished.set)
        else:
            # no worker, run on current thread
            self._finished.set()     

    def _run_data_callbacks(self, a_data_response: CoreResponse):
        for callback in self._data_callbacks:
            try:
                if self._processor_thread:
                    # delegate to given worker
                    self._processor_thread.run(partial(callback, a_data_response))
                else:
                    # no worker, run on current thread
                    callback(a_data_response)
            except Exception as err:
                logging.error("Error processing response: %s", err)
                logging.error(" Request: %s", self)
                logging.error(" Response: %s", a_data_response)

    def process_response(self, a_response:CoreResponse):
        if self._processor_thread:
            # run in the request processing thread
            self._processor_thread.run(self._process_response, a_response)
        else:
            # run in receive thread, may delay messaging processing!
            self._process_response(a_response)

    def _process_response(self, a_response:CoreResponse):
        try:
            # Call a generic parser for incoming response data
            a_response.parse()

            if a_response.response_type == CoreResponse.ResponseType.STATUS:
                self.status = a_response.response_type
                self.status_content = a_response.content
                self._core_status = a_response.core_status
                self._data = a_response.data # set parsed data in request

                # Request is marked as finished after processing callbacks
                self._run_status_callbacks()

            if a_response.response_type == CoreResponse.ResponseType.DATA:
                self._run_data_callbacks(a_response)

        except Exception as err:
            logging.error("Error processing response: %s", err)
            logging.error(" Request: %s", self)
            logging.error(" Response: %s", a_response)
