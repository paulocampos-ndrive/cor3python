from ctypes import CDLL
from ctypes import c_char_p
import os
import logging


class CommonCode:
    def __init__(self, a_cc_lib_path: str, a_debug_tokens: str = ""):
        self._instructions_resources_loaded = None
        self._voice_loaded = None
        self._lib = None
        
        self._lib_path = a_cc_lib_path
        self._debug_tokens = a_debug_tokens

        if not os.path.isfile(self._lib_path):
            logging.error("File not found:'{0}'".format(
                self._lib_path))
            return 

        # Ctypes lib interface
        self._lib = CDLL(self._lib_path)

        # Register debug tokens
        self._lib.SoundManagerJig_DebugRegisterTokens(
            self._debug_tokens.encode('utf-8'))

        # Extra initialize steps...
        if not self._lib.SoundManagerJig_Construct():
            logging.info("Error: SoundManagerJig_Construct")


    def load_instructions_resource(self, a_instructions_resource_file):
        if not self._lib:
            logging.error("load_instructions_resource: mi9cc not initialized")

        if self._lib.SoundManagerJig_OpenInstructionsResource(
                a_instructions_resource_file.encode('utf-8')):
            self._instructions_resources_loaded = a_instructions_resource_file
            return True
        else:
            self._instructions_resources_loaded = None
            logging.error(
                "load_instructions_resource: SoundManagerJig_OpenInstructionsResource"
            )
            return False

    def load_voice(self, a_voice_file: str) -> bool:
        if not self._lib:
            logging.error("load_voice: mi9cc not initialized")

        if self._lib.SoundManagerJig_OpenVoice(a_voice_file.encode('utf-8')):
            self._voice_loaded = a_voice_file
            return True
        else:
            self._voice_loaded = None
            logging.error("load_voice: SoundManagerJig_OpenVoice")
            return False            

    def __del__(self):
        # Extra finalize steps for cc lib
        if self._lib is not None:
            self._lib.SoundManagerJig_CloseInstructionsResource()
            self._lib.SoundManagerJig_CloseSystemVoice()

    def translate_smart_roadbook_entry(self, a_roadbook_raw_str: str) -> str:
        func = self._lib.SoundManagerJig_TranslateSmartRoadBookEntry
        func.restype = c_char_p
        return func(a_roadbook_raw_str.encode('utf-8')).decode()
