from threading import Lock
from typing import Dict, List, Callable, Tuple, Optional

from cor3python.messaging.core_response import CoreResponse
from cor3python.messaging.core_request import CoreRequest
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.utils.thread_manager import thread_manager
from cor3python.utils.log import log_api_error
from cor3python.core_objects.base import Base
from cor3python.core_objects.api.routing import APIRouting
from cor3python.core_objects.Itinerary import Itinerary
from cor3python.core_objects.common.routing.profile import Profile
from cor3python.core_objects.common.routing.waypoint import WayPoint


class Routing(Base):

    def __init__(self, a_messaging: BaseDispatcher, a_thread=thread_manager.get_thread("ROUTING")):
        super().__init__()

        self._messaging = a_messaging

        # Create API Level object
        self._api = APIRouting(a_messaging, a_thread)

        # Profiles Cache
        self._profiles_lock = Lock()
        self._profiles : Dict[str, Profile] = dict()

        self._cached_profiles: Dict[str, Profile] = dict()
        
        # Optimal waypoints order
        self._original_order: dict = dict()
        self._optimal_order: list = list()      

        # Matrix Results
        self._matrix_results: List[Dict] = list()

    def create_profile(self, a_profile: Profile) -> Profile:
        """Create a new profile, or returns if already available one

        Args:
            a_profile (Profile): Profile to be created

        Returns:
            Profile created or None in case of error
        """

        if a_profile.name in self._profiles:
            return self._profiles[a_profile.name]
        
        api_result = self._api.create_profile(a_profile)

        if not api_result.status:
            log_api_error(api_result)
            raise Exception(api_result.core_status)

        # save new data into cache
        self._profiles_lock.acquire()
        self._profiles[a_profile.name] = a_profile
        self._profiles_lock.release()
        return a_profile

    def destroy_profile(self, a_profile_name: str) -> bool:
        """Destroy profile

        Args:
            a_profile_name (str): Profile name

        Returns:
            bool: success
        """

        if a_profile_name not in self._profiles:
            return False

        api_result = self._api.destroy_profile(a_profile_name)

        if not api_result.status:
            log_api_error(api_result)
            return False
        
        # remove profile from cache
        self._profiles_lock.acquire()
        del self._profiles[a_profile_name]
        self._profiles_lock.release()
        return True

    def create_itinerary(self, a_itinerary_name: str) -> Itinerary:
        """Create a new Itinerary

        Args:
            a_itinerary_name (str): Itinerary Name

        Returns:
            Itinerary created or None in case of error
        """

        api_result = self._api.create_itinerary(a_itinerary_name)

        if not api_result.status:
            log_api_error(api_result)
            raise Exception(api_result.core_status)

        return Itinerary(self._messaging, a_itinerary_name=a_itinerary_name)

    def add_graph_cache_profile(self, a_profile: Profile) -> bool:
        if a_profile.name in self._cached_profiles:
            return True

        api_result = self._api.add_graph_cache_profile(a_profile.full_name)

        if not api_result.status:
            log_api_error(api_result)
            return False

        self._profiles_lock.acquire()
        self._cached_profiles[a_profile.name] = a_profile
        self._profiles_lock.release()
        return True

    def remove_graph_cache_profile(self, a_profile: Profile) -> bool:
        if a_profile not in self._cached_profiles:
            return False

        api_result = self._api.remove_graph_cache_profile(a_profile.full_name)

        if not api_result.status:
            log_api_error(api_result)
            return False
        
        # remove profile from cache
        self._profiles_lock.acquire()
        del self._cached_profiles[a_profile.name]
        self._profiles_lock.release()
        return True

    def get_optimal_waypoints_order(self,
                                    a_waypoints_list: List[WayPoint],                                    
                                    a_monitor_as_origin: bool = False,
                                    a_monitor_as_destination: bool = False,
                                    a_lock_first_waypoint: bool = False,
                                    a_lock_last_waypoint: bool = False,
                                    a_use_optimal_algorithm: bool = True,
                                    a_monitor=None) -> list:

        self._optimal_order = list()

        api_result = self._api.get_optimal_waypoints_order(a_waypoints_list = a_waypoints_list,                                    
                                    a_data_cb = self.process_optimal_waypoints_order_data_response , 
                                    a_status_cb = self.process_optimal_waypoints_order_status_response,
                                    a_monitor_as_origin = a_monitor_as_origin,
                                    a_monitor_as_destination = a_monitor_as_destination,
                                    a_lock_first_waypoint = a_lock_first_waypoint,
                                    a_lock_last_waypoint = a_lock_last_waypoint,
                                    a_use_optimal_algorithm = a_use_optimal_algorithm,
                                    a_monitor = a_monitor)
        
        if not api_result.status:
            log_api_error(api_result)

        return api_result

    def process_optimal_waypoints_order_data_response(self, a_core_response: CoreResponse):
        pass

    def process_optimal_waypoints_order_status_response(self, a_request: CoreRequest):
        self._optimal_order = a_request.data

    def get_optimal_waypoints_order_result(self):
        return self._optimal_order        

    def get_distance_matrix(self,
                            a_origins_list: List[WayPoint], 
                            a_destinations_list: List[WayPoint],                                    
                            a_profile: str,
                            a_fast_calc: bool = False,
                            a_number_workers: int = 2):
        
        self._matrix_results = list()

        api_result = self._api.get_distance_matrix(a_origins_list, 
                                                a_destinations_list,                                    
                                                a_profile,
                                                self.process_distance_matrix_data_response, 
                                                self.process_distance_matrix_status_response,
                                                a_fast_calc,

                                                a_number_workers)

        if not api_result.status:
            log_api_error(api_result)

        return api_result

    def process_distance_matrix_data_response(self, a_core_response: CoreResponse):
        self._matrix_results.append(a_core_response.data)

    def process_distance_matrix_status_response(self, a_request: CoreRequest):
        pass

    def get_distance_matrix_results(self):
        return self._matrix_results     