from threading import Lock
from typing import Optional

from cor3python.core_objects.base import Base
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.utils.thread_manager import thread_manager
from cor3python.core_objects.api.api_status import ApiStatus
from cor3python.utils.log import log_api_error
from cor3python.core_objects.api.environment import APIEnvironment
from cor3python.core_objects.common.environment.system_info import SystemInfo
from cor3python.core_objects.common.environment.core_versions import CoreVersions
from cor3python.core_objects.common.environment.memory_information import MemoryInformation
from cor3python.messaging.status_code import Cor3Status


class Environment(Base):
    """
    CORE Environment Instance
    """

    def __init__(self, a_messaging: BaseDispatcher, a_thread=thread_manager.get_thread("ENVIRONMENT")):
        super().__init__()

        # Create API Level object
        self._api = APIEnvironment(a_messaging, a_thread)

        # properties
        self._system_info : Optional[SystemInfo] = None
        self._core_versions : Optional[CoreVersions] = None

    def get_system_info(self, a_force_update : bool=False) -> SystemInfo:
        """Get System Information

        Returns:
            ProductInfo: ProductInfo instance or None on error
        """
        if self._system_info is not None and not a_force_update:
            return self._system_info

        sys_info = SystemInfo()

        api_result = self._api.get_system_info(
            a_data_cb=sys_info.process_data_response
        )

        if not api_result.status:
            log_api_error(api_result)
            raise Exception(api_result.core_status)
        
        self._system_info = sys_info
        return sys_info

    def get_versions(self, a_force_update : bool=False) -> CoreVersions:
        """Get Core Versions

        Returns:
            CoreVersions: CoreVersions instance or None on error
        """
        if self._core_versions is not None and not a_force_update:
            return self._core_versions

        _versions = CoreVersions()

        api_result = self._api.get_versions(
            a_data_cb=_versions.process_data_response
        )

        if not api_result.status:
            log_api_error(api_result)
            raise Exception(api_result.core_status)
        
        self._core_versions = _versions
        return _versions        

    def get_memory_information(self) -> CoreVersions:
        """Get Memory Information

        Returns:
            Memory Information
        """

        _info = MemoryInformation()

        api_result = self._api.get_memory_information(
            a_data_cb=_info.process_data_response
        )

        if not api_result.status:
            log_api_error(api_result)
            raise Exception(api_result.core_status)
        
        return _info