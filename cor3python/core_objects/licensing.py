import os
from cor3python.core_objects.base import Base
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.utils.thread_manager import thread_manager
from cor3python.core_objects.api.api_status import ApiStatus
from cor3python.utils.log import log_api_error
from cor3python.core_objects.api.licensing import APILicensing
from cor3python.core_objects.common.licensing.product_info import ProductInfo
from cor3python.messaging.status_code import Cor3Status


class Licensing(Base):

    def __init__(self, a_messaging: BaseDispatcher, a_thread=thread_manager.get_thread("LICENSING")):
        super().__init__()
        
        # Create API Level object
        self._licensing = APILicensing(a_messaging, a_thread)

        # properties
        self._loaded_products = {}

    def get_product_info(self, a_filename: str) -> ProductInfo:
        """Get details from a a valid cor3 product

        Args:
            a_filename (str): filename from the products

        Returns:
            ProductInfo: ProductInfo instance or None on error
        """
        ret = self._licensing.get_product_info(os.path.abspath(os.path.expanduser(a_filename)))
        if not ret.status:
            return None
        return ProductInfo().load(ret.request.data)

    def add_products(self, a_products: list) -> list:
        """Add Products from a list of products

        Args:
            a_products (list): products list to be loaded

        Returns:
            list: list of booleans for each product
        """
        products_list = list()
        for p in a_products:
            products_list.append([p.sku, p.filename])
        ret = self._licensing.add_products(products_list)
        if not ret.status:
            return [False*len(products_list)]

        # save the products that were loaded
        for i,product in enumerate(a_products):
            if ret.request.data[i] == Cor3Status.kErrNone:
                self._loaded_products[product.sku] = product

        return list(map(lambda x: x == Cor3Status.kErrNone, ret.request.data))

    def remove_products(self, a_products: list) -> list:
        """Remove Products from a list of products

        Args:
            a_products (list): products list to be removed

        Returns:
            list: list of booleans for each product
        """
        products_list = list()
        for p in a_products:
            products_list.append(p.sku)
        ret = self._licensing.remove_products(products_list)
        if not ret.status:
            return [False*len(products_list)]

        # remove the products from the loaded products list
        for i,product in enumerate(a_products):
            if ret.request.data[i] == Cor3Status.kErrNone and product.sku in self._loaded_products:
                del self._loaded_products[product.sku]

        return list(map(lambda x: x == Cor3Status.kErrNone, ret.request.data))


    def add_product_folder(self, a_folder: str) -> list:
        """Load every valid product found in folder and sub-folder

        Args:
            a_folder (str): path to the folder

        Returns:
            list: List of products loaded
        """
        prods = list()
        dir_ = os.path.abspath(os.path.expanduser(a_folder))
        for f in os.listdir(dir_):
            path = os.path.join(dir_, f)
            if not os.path.isfile(path):
                continue
            product = self.get_product_info(path)
            if product:
                prods.append(product)

        result = self.add_products(prods)
        # TODO: Improve return to check for errors
        return prods