from threading import Lock
from typing import Optional

from cor3python.core_objects.base import Base
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.utils.thread_manager import thread_manager
from cor3python.core_objects.api.api_status import ApiStatus
from cor3python.utils.log import log_api_error
from cor3python.core_objects.api.tileset import APITileset
from cor3python.messaging.status_code import Cor3Status


class TileSet(Base):
    """
    CORE VectorTiles Instance
    """

    def __init__(self, a_name:str, a_type:str, a_scheme:str, a_pipeline_count:int, a_folder:str, a_messaging: BaseDispatcher, a_thread=thread_manager.get_thread("VECTOR_TILES")):
        super().__init__()

        self._name = a_name
        self._type = a_type
        self._scheme = a_scheme
        self._pipeline_count = a_pipeline_count
        self._folder = a_folder

        # Create API Level object
        self._api = APITileset(a_name=a_name, a_messaging=a_messaging, a_thread=a_thread)

        # properties
        self._max_simplex_zoom: int = None
        self._properties['world_encoder_enabled'] = False
        self._properties['world_names_encoder_enabled'] = False
        self._properties['world_line_encoder_enabled'] = False
        self._properties['multi_polygon_encoder_enabled'] = False
        self._properties['polygon_names_encoder_enabled'] = False
        self._properties['edge_encoder_enabled'] = False
        self._properties['settlement_encoder_enabled'] = False
        self._properties['line_encoder_enabled'] = False
        self._properties['building_encoder_enabled'] = False
        self._properties['show_partial_progress'] = False
        self._properties['pipeline_batch_size'] = 10
    
    ###########################################
    @property
    def world_encoder_enabled(self) -> bool:
        return self._properties["world_encoder_enabled"]
    @world_encoder_enabled.setter
    def world_encoder_enabled(self, a_value: bool):
        self._property_setter_bool("world_encoder_enabled", a_value)
    ###########################################
    @property
    def world_names_encoder_enabled(self) -> bool:
        return self._properties["world_names_encoder_enabled"]
    @world_names_encoder_enabled.setter
    def world_names_encoder_enabled(self, a_value: bool):
        self._property_setter_bool("world_names_encoder_enabled", a_value)
    ###########################################
    @property
    def world_line_encoder_enabled(self) -> bool:
        return self._properties["world_line_encoder_enabled"]
    @world_line_encoder_enabled.setter
    def world_line_encoder_enabled(self, a_value: bool):
        self._property_setter_bool("world_line_encoder_enabled", a_value)
    ###########################################
    @property
    def multi_polygon_encoder_enabled(self) -> bool:
        return self._properties["multi_polygon_encoder_enabled"]
    @multi_polygon_encoder_enabled.setter
    def multi_polygon_encoder_enabled(self, a_value: bool):
        self._property_setter_bool("multi_polygon_encoder_enabled", a_value)
    ###########################################
    @property
    def polygon_names_encoder_enabled(self) -> bool:
        return self._properties["polygon_names_encoder_enabled"]
    @polygon_names_encoder_enabled.setter
    def polygon_names_encoder_enabled(self, a_value: bool):
        self._property_setter_bool("polygon_names_encoder_enabled", a_value)        
    ###########################################
    @property
    def edge_encoder_enabled(self) -> bool:
        return self._properties["edge_encoder_enabled"]
    @edge_encoder_enabled.setter
    def edge_encoder_enabled(self, a_value: bool):
        self._property_setter_bool("edge_encoder_enabled", a_value)             
    ###########################################
    @property
    def settlement_encoder_enabled(self) -> bool:
        return self._properties["settlement_encoder_enabled"]
    @settlement_encoder_enabled.setter
    def settlement_encoder_enabled(self, a_value: bool):
        self._property_setter_bool("settlement_encoder_enabled", a_value)               
    ###########################################
    @property
    def line_encoder_enabled(self) -> bool:
        return self._properties["line_encoder_enabled"]
    @line_encoder_enabled.setter
    def line_encoder_enabled(self, a_value: bool):
        self._property_setter_bool("line_encoder_enabled", a_value)             
    ###########################################
    @property
    def building_encoder_enabled(self) -> bool:
        return self._properties["building_encoder_enabled"]
    @building_encoder_enabled.setter
    def building_encoder_enabled(self, a_value: bool):
        self._property_setter_bool("building_encoder_enabled", a_value)             
    ###########################################
    @property
    def show_partial_progress(self) -> bool:
        return self._properties["show_partial_progress"]
    @show_partial_progress.setter
    def show_partial_progress(self, a_value: bool):
        self._property_setter_bool("show_partial_progress", a_value)               
    ###########################################
    @property
    def pipeline_batch_size(self) -> int:
        return self._properties["pipeline_batch_size"]
    @pipeline_batch_size.setter
    def pipeline_batch_size(self, a_value: int):
        self._property_setter_int("pipeline_batch_size", a_value)

    def set_configuration_json(self, a_json: str) -> bool:
        """Set configurarion json

        Returns bool: Success / Fail
        """
        api_result = self._api.set_configuration_json(a_json=a_json)
        if not api_result.status:
            log_api_error(api_result)
            return False
        return True

    def set_max_simplex_zoom(self, a_max_zoom: int) -> ApiStatus:
        """Set configurarion json

        Returns bool: Success / Fail
        """
        api_result = self._api.set_max_simplex_zoom(a_max_zoom=a_max_zoom)
        if not api_result.status:
            log_api_error(api_result)
            return False
        self._max_simplex_zoom = a_max_zoom
        return True

    def get_tile(self, a_x: int, a_y: int, a_z: int) -> str:
        """Get a Tile for a Z/Y/Z

        Returns None in error or BASE64 str with tile
        """
        api_result = self._api.get_tile(a_x=a_x, a_y=a_y, a_z=a_z)
        if not api_result.status:
            log_api_error(api_result)
            return None
        
        if len(api_result.request.data) > 0:
            return api_result.request.data[0]
        else:
            return None