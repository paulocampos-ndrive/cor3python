import os
from typing import Dict, List, Callable, Tuple, Optional

from cor3python.core_objects.base import Base
from cor3python.messaging.core_response import CoreResponse
from cor3python.messaging.core_request import CoreRequest
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.utils.thread_manager import thread_manager
from cor3python.core_objects.api.api_status import ApiStatus
from cor3python.utils.log import log_api_error
from cor3python.core_objects.api.data import APIData
from cor3python.messaging.status_code import Cor3Status
from cor3python.core_objects.common.types.point_2d import Point2D
from cor3python.core_objects.common.data.edge_basic_info import EdgeBasicInfo


class Data(Base):

    def __init__(self, a_messaging: BaseDispatcher, a_thread=thread_manager.get_thread("LICENSING")):
        super().__init__()
        
        # Create API Level object
        self._api = APIData(a_messaging, a_thread)

        # internal results
        self._edge_basic_info: List = list()

    def get_edge_basic_info(self, a_locale: str, a_points: List[Point2D]):
        """Gets edge information from a list of multiple points
        Args:
            a_points (list): List of points

        Returns:
            ApiStatus: ApiStatus
        """
        self._edge_basic_info = list()
        ret = self._api.get_edge_basic_info(a_locale=a_locale, a_points=a_points, a_status_cb=self.process_edge_basic_info_status_response, a_data_cb=self.process_edge_basic_info_data_response)
        if not ret.status:
            return []
        
        return self._edge_basic_info

    def process_edge_basic_info_data_response(self, a_core_response: CoreResponse):
        pass

    def process_edge_basic_info_status_response(self, a_request: CoreRequest):
        self._edge_basic_info = a_request.data