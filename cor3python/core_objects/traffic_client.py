from cor3python.core_objects.base import Base
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.utils.thread_manager import thread_manager
from cor3python.core_objects.api.api_status import ApiStatus
from cor3python.utils.log import log_api_error
from cor3python.core_objects.api.traffic_client import APITrafficClient
from cor3python.messaging.status_code import Cor3Status


class TrafficClient(Base):

    def __init__(self, a_messaging: BaseDispatcher, a_thread=thread_manager.get_thread("TRAFFIC_CLIENT")):
        super().__init__()
        
        # Create API Level object
        self._api = APITrafficClient(a_messaging, a_thread)

        # properties
        self._initialized: bool = False
        self._hostname: str = None
        self._port: int = None
        self._config: str = None
        self._client_type: str = None
        self._dummy_mode: bool = False
        self._dummy_traffic_data_file: str = None

    def setup_configuration(self,
                            a_hostname: str,
                            a_port: int,
                            a_config: str,
                            a_client_type: str = ""):

        """Setup Traffic server connection

        Returns:
            bool: Success or Fail
        """
        ret = self._api.setup_configuration(a_hostname=a_hostname, a_port=a_port, a_config=a_config, a_client_type=a_client_type)
        if not ret.status:
            self._initialized = False
            return False

        self._hostname = a_hostname
        self._port = a_port
        self._config = a_config
        self._client_type = a_client_type
        self._initialized = True            
        return True
