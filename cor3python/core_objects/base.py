from cor3python.utils.log import log_api_error
from cor3python.utils.parsing import bool2str


class Base:
    def __init__(self) -> None:
        # API Object
        self._api = None

        # Properties
        self._properties = dict()

    def _property_setter_bool(self, a_property: str, a_value: bool):
        if a_value == self._properties[a_property]:
            return
        result = self._api.set({"{}".format(a_property):"{}".format(bool2str(a_value))})
        if not result.status:
            log_api_error(result)
            return
        self._properties[a_property] = a_value

    def _property_setter_int(self, a_property: str, a_value: int):
        if a_value == self._properties[a_property]:
            return
        result = self._api.set({"{}".format(a_property):"{}".format(a_value)})
        if not result.status:
            log_api_error(result)
            return
        self._properties[a_property] = a_value


    def destroy(self):
        self._api.destroy()
    
    @property
    def api(self):
        return self._api