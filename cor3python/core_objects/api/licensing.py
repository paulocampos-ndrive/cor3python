from cor3python.core_objects.api.api_base import ApiBase
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.utils.thread_manager import thread_manager
from cor3python.core_objects.api.api_status import ApiStatus
from cor3python.messaging.core_request import CoreRequest
from cor3python.messaging.status_code import Cor3Status


class APILicensing(ApiBase):

    def __init__(self, a_messaging: BaseDispatcher, a_thread=thread_manager.get_thread("LICENSING")):
        super().__init__(a_messaging=a_messaging, a_thread=a_thread)
        self.name = 'Licensing'

    def add_products(self, a_products:list) -> ApiStatus:
        str_products = ""
        for i,p in enumerate(a_products):
            if i > 0:
                str_products += ','
            str_products += "[\"{}\",\"{}\"]".format(p[0], p[1])
        str_products = "[{}]".format(str_products)
        message = r'{}.AddProducts {}'.format(self.full_name, str_products)
        req = CoreRequest(self._thread)
        req.message = message
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def remove_products(self, a_sku_list:list) -> ApiStatus:
        str_products = ""
        for i,p in enumerate(a_sku_list):
            if i > 0:
                str_products += ','
            str_products += "\"{}\"".format(p)
        message = r'{}.RemoveProducts [{}]'.format(self.full_name, str_products)
        req = CoreRequest(self._thread)
        req.message = message
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def get_product_info(self, a_filename:str) -> ApiStatus:
        message = r'{}.GetProductInfo "{}"'.format(self.full_name, a_filename)
        req = CoreRequest(self._thread)
        req.message = message
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)
