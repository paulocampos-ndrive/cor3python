from cor3python.messaging.core_request import CoreRequest


class ApiStatus:

    def __init__(self, a_status: bool, a_request:CoreRequest, a_description: str = ""):
        """Cor3Pythin API return status class

        Args:
            a_status (bool): Generic pass/fail result
            a_request (CoreRequest): Return from the original cor3 request.
            a_description (str, optional): Any description for the result given. Defaults to "".
        """
        self._status = a_status
        self._request = a_request
        self._core_status = a_request.core_status
        self._description = a_description

    @property
    def status(self):
        return self._status

    @property
    def request(self):
        return self._request

    @property
    def core_status(self):
        return self._core_status

    @property
    def description(self):
        return self._description
