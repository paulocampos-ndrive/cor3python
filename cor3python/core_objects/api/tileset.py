from cor3python.core_objects.api.api_base import ApiBase
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.utils.thread_manager import thread_manager
from cor3python.core_objects.api.api_status import ApiStatus
from cor3python.messaging.core_request import CoreRequest
from cor3python.messaging.status_code import Cor3Status


class APITileset(ApiBase):

    def __init__(self, a_name: str, a_messaging: BaseDispatcher, a_parent: str = "VectorTiles", a_thread=thread_manager.get_thread("VECTOR_TILES")):
        super().__init__(a_messaging=a_messaging, a_thread=a_thread)
        self.parent = a_parent
        self.name = a_name

    def set_configuration_json(self, a_json: str, a_status_cb= None, a_data_cb= None) -> ApiStatus:
        message = r'{}.SetConfigurationJSON "{}"'.format(self.full_name, a_json)
        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def set_max_simplex_zoom(self, a_max_zoom: int, a_status_cb= None, a_data_cb= None) -> ApiStatus:
        message = r'{}.SetMaxSimplexZoom {}'.format(self.full_name, a_max_zoom)
        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)   

    def get_tile(self, a_x: int, a_y: int, a_z: int, a_status_cb= None, a_data_cb= None) -> ApiStatus:
        message = r'{}.GetTile {},{},{}'.format(self.full_name, a_z, a_x, a_y)
        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)   

        