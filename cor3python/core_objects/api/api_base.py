from cor3python.utils.thread_manager import WorkerThread, thread_manager
from cor3python.messaging.core_request import CoreRequest
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.messaging.status_code import Cor3Status
from cor3python.core_objects.api.api_status import ApiStatus


class ApiBase:

    def __init__(self, a_messaging:BaseDispatcher, a_thread: WorkerThread=None):

        self.messaging = a_messaging

        # Object identification
        self._object_name = None
        self._parent_name = None
        self._full_object_name = None

        # Processing Thread
        self._stop_thread_on_destroy = False
        if a_thread:
            self._thread = a_thread
        else:
            # get a temporary thread for this object
            self._thread = thread_manager.get_thread()
            self._stop_thread_on_destroy = True

    def __del__(self):
        if self._stop_thread_on_destroy:
            thread_manager.remove(self._thread)

    @property
    def parent(self) -> str:
        return self._parent_name

    @parent.setter
    def parent(self, a_value: str) -> str:
        self._parent_name = str(a_value)
        self._build_full_name()

    @property
    def name(self) -> str:
        """Object name, without parent

        Returns:
            str: Object name
        """
        return self._object_name

    @name.setter
    def name(self, a_value: str) -> str:
        self._object_name = str(a_value)
        self._build_full_name()

    def _build_full_name(self):
        if self.parent is None:
            self._full_object_name = self._object_name
        else:
            self._full_object_name = "{}.{}".format(self._parent_name, self._object_name)

    @property
    def full_name(self) -> str:
        """Full object name, including parenting

        Returns:
            [str]: object full name
        """
        return self._full_object_name

    def set(self, a_dict: dict) -> ApiStatus:
        """Set's object attributes key -> value

        Args:
            a_dict (dict): key -> value of attributes to be set

        Returns:
            bool: success/failure
        """
        pairs = ["{}:{}".format(k, v) for k, v in a_dict.items()]
        message = """{}.Set {{ {} }}""".format(self.full_name, ','.join(pairs))
        req = CoreRequest(self._thread)
        req.message = message
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def destroy(self):
        """Destroy core object
        """
        message = """{}.Destroy""".format(self.full_name)
        req = CoreRequest(self._thread)
        req.message = message
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)
