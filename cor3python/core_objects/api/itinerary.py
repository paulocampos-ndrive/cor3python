from typing import List, Optional
from cor3python.core_objects.api.api_base import ApiBase
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.utils.thread_manager import thread_manager
from cor3python.core_objects.api.api_status import ApiStatus
from cor3python.messaging.core_request import CoreRequest
from cor3python.messaging.status_code import Cor3Status
from cor3python.core_objects.common.routing.waypoint import WayPoint
from cor3python.core_objects.common.routing.route_calculation import RouteCalculation
from cor3python.utils.parsing import bool2str


class APIItinerary(ApiBase):

    def __init__(self, a_messaging: BaseDispatcher,  a_itinerary_name: str, a_parent: str = "Routing", a_thread=thread_manager.get_thread("ITINERARY")):
        super().__init__(a_messaging=a_messaging, a_thread=a_thread)

        self.parent = a_parent
        self.name = a_itinerary_name

    def add_waypoint(self, a_waypoint: WayPoint) -> ApiStatus:
        search_id_str = ""
        if a_waypoint.search_id is not None:
            search_id_str = ",search_id:" + str(a_waypoint.search_id)

        message = "{}.AddWaypoint {},{{location:{} {}}}".format(
            self.full_name, a_waypoint.name, a_waypoint.point.get_core_str(), search_id_str)
        req = CoreRequest(self._thread)
        req.message = message
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def clear(self) -> ApiStatus:
        message = "{}.Clear ".format(self.full_name)
        req = CoreRequest(self._thread)
        req.message = message
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def calculate(self, a_route_calculation: RouteCalculation, a_data_cb, a_status_cb, a_timeout: int = None) -> ApiStatus:
        calculate_options = "profile:{},".format(
            a_route_calculation.profile.full_name)

        # number of alternatives
        calculate_options += "alternatives:{},".format(
            a_route_calculation.alternatives)

        # progress info
        calculate_options += "progress:{},".format(
            bool2str(a_route_calculation.progress_enable))

        # calc_timestamp
        if a_route_calculation.calc_timestamp is not None:
            calculate_options += "calc_timestamp:{},".format(
                a_route_calculation.calc_timestamp)

        # extensive_lane_info
        calculate_options += "extensive_lane_info:{},".format(
            bool2str(a_route_calculation.extensive_lane_info))

        # remove last ,
        calculate_options = calculate_options[:-1]

        message = "{}.Calculate {{ {} }}".format(
            self.full_name, calculate_options)
        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait(a_timeout)
        if req.core_status is None:
            raise Exception("TIMEOUT")
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def export_itinerary(self, a_simplify_meters: int, a_format: str = 'CompressedLine',  a_data_cb=None, a_status_cb=None) -> ApiStatus:
        message = """{}.ExportItinerary {{ simplify_distance:{}, format:"{}" }}""".format(
            self.full_name, a_simplify_meters, a_format)
        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def init_from_itinerary_alternative(self, a_main_itinerary_name: str, a_alternative_number: int, a_data_cb, a_status_cb) -> ApiStatus:
        message = "{}.InitFromItineraryAlternative {}, {}".format(
            self.full_name, a_main_itinerary_name, a_alternative_number)
        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def get_smart_roadbook(self, a_first_route: int, a_last_route: int, a_data_cb, a_status_cb, a_mode: str = "ToManeuver",
                           a_lane_info: bool = False, a_traffic_minimum_severity: int = 0,
                           a_locale: str = None,
                           a_monitor: str = None,
                           a_warnings: Optional[List[str]] = None):
        

        lane_info = bool2str(a_lane_info)
        mode = "\"{0}\"".format(a_mode)

        monitor = ""
        if a_monitor is not None:
            monitor = ", monitor:{{ID:{}, use_location:yes, use_traffic:yes}}".format(
                a_monitor)

        warnings = ""
        if a_warnings and len(a_warnings) > 0:
            warnings = "warnings: []"
            for warning in a_warnings:
                warnings += """ "{}",""".format(warning)
            warnings = warnings[:-1]
            warnings += "]"    

        locale = ""
        if a_locale:
            locale = """, locale:"{}" """.format(a_locale)            

        message = "{}.GetSmartRoadbook {{ first_route_index: {}, last_route_index: {}, lane_info: {}, traffic_minimum_severity:{}, mode: {} {} {} {} }}".format(
            self.full_name, a_first_route, a_last_route, lane_info, a_traffic_minimum_severity, mode, monitor, warnings, locale)
        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)


