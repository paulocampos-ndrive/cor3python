from threading import Lock

from cor3python.utils.parsing import bool2str
from cor3python.messaging.core_response import CoreResponse
from cor3python.core_objects.common.types.point_2d import Point2D
from cor3python.core_objects.api.api_base import ApiBase
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.utils.json_parser import from_string
from cor3python.utils.thread_manager import thread_manager
from cor3python.core_objects.api.api_status import ApiStatus
from cor3python.messaging.core_request import CoreRequest
from cor3python.messaging.status_code import Cor3Status
from cor3python.core_objects.common.search.search_filters import (
    RecordTypeFilter,
    SearchFilters,
)


class APISearch(ApiBase):
    INSTANCE_COUNTER = 0
    _instance_counter_lock = Lock()

    def __init__(self, a_messaging: BaseDispatcher, a_name: str = None):
        if a_name is None:
            APISearch._instance_counter_lock.acquire()
            APISearch.INSTANCE_COUNTER += 1
            APISearch._instance_counter_lock.release()
            name = "Search_{}".format(APISearch.INSTANCE_COUNTER)
        else:
            name = a_name

        # super().__init__(a_messaging=a_messaging, a_thread=thread_manager.get_thread(name.upper()))
        super().__init__(
            a_messaging=a_messaging, a_thread=thread_manager.get_thread("SEARCH")
        )

        self._new_search(name)
        self.name = name

    def _new_search(self, a_name: str) -> bool:
        message = r"CreateSearch {}".format(a_name)
        req = CoreRequest(self._thread)
        req.message = message
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return True
        raise Exception(req.core_status)

    def destroy(self):
        message = r"DestroySearch {}".format(self.name)
        req = CoreRequest(self._thread)
        req.message = message
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return True
        raise Exception(req.core_status)

    def stop(self) -> ApiStatus:
        """Stop current search operation

        Returns:
            ApiStatus: Status
        """
        message = r"{}.Stop ".format(self.full_name)
        req = CoreRequest(self._thread)
        req.message = message
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone, Cor3Status.kErrSearchNotRunning]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def set_locale(self, a_locale: str) -> ApiStatus:
        """Sets search locale

        Args:
            a_locale (str): language code. ex: en

        Returns:
            ApiStatus: Status
        """
        message = r'{}.SetLocale "{}"'.format(self.full_name, a_locale)
        req = CoreRequest(self._thread)
        req.message = message
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def set_output_format(self, a_format: dict) -> ApiStatus:
        """Set Output format for the bext search API calls

        Args:
            a_format (dict): Dict with key for result type. Value for column names (from SearchOutputTypes)

        Returns:
            ApiStatus: ApiStatus
        """
        outformat = "{"
        for t in a_format:
            if t in ["E"]:
                continue

            outformat += "{0}: [".format(t)
            for col_type in a_format[t]:
                outformat += "{0},".format(col_type.__name__)
            outformat = outformat[:-1]
            outformat += "],"
        outformat = outformat[:-1]
        outformat += "}"
        message = """{}.SetOutputFormat {}""".format(self.full_name, outformat)
        req = CoreRequest(self._thread)
        req.message = message
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def get(self, a_ids: list, a_status_cb, a_data_cb) -> ApiStatus:
        """Runs a Get operation in Search
        Args:
            a_ids (list): List of ids to be returned
            a_status_cb ([callable]): status message processor
            a_data_cb ([callable]): data message processor

        Returns:
            ApiStatus: ApiStatus
        """
        message = """{}.Get [{}]""".format(self.full_name, ",".join(a_ids))
        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def start_full_search(
        self,
        a_search_string: str,
        a_search_filter: SearchFilters,
        a_status_cb,
        a_data_cb,
    ) -> ApiStatus:
        """Runs a FullSearch operation in Search
        Args:
            a_search_string (str): String to be searched
            a_search_filter (SearchFilters): Search Filters definition
            a_status_cb ([callable]): status message processor
            a_data_cb ([callable]): data message processor

        Returns:
            ApiStatus: ApiStatus
        """
        message = """{}.StartFullSearch "{}", {} """.format(
            self.full_name, a_search_string, a_search_filter
        )
        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def start_search(
        self,
        a_search_string: str,
        a_search_filter: SearchFilters,
        a_status_cb,
        a_data_cb,
    ) -> ApiStatus:
        """Runs a Start operation in Search
        Args:
            a_search_string (str): String to be searched
            a_search_filter (SearchFilters): Search Filters definition
            a_status_cb ([callable]): status message processor
            a_data_cb ([callable]): data message processor

        Returns:
            ApiStatus: ApiStatus
        """
        message = """{}.Start "{}", {} """.format(
            self.full_name, a_search_string, a_search_filter
        )
        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def geocode(
        self,
        a_search_dict: dict,
        a_record_type: RecordTypeFilter,
        a_status_cb,
        a_data_cb,
        a_try_partial_words: bool = True
    ) -> ApiStatus:
        """Runs a Geocode operation in Search
        Args:
            a_search_dict (dict): dict with terms to be used in geocoding
            a_record_type (RecordTypeFilter): record types to be returned
            a_status_cb ([callable]): status message processor
            a_data_cb ([callable]): data message processor
            a_try_partial_words (bool): Should try partial words match? Default: True

        Returns:
            ApiStatus: ApiStatus
        """
        filters = "{"
        for k, v in a_search_dict.items():
            filters += '{}:"{}",'.format(k, v)
        filters += str(a_record_type)
        
        filters += ',{}:{}'.format('try_partial_words', bool2str(a_try_partial_words))
        
        filters += "}"
        message = """{}.Geocoding {} """.format(self.name, filters)
        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone, Cor3Status.kErrGeocodingCancelled, Cor3Status.kErrGeocodingNotFound]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def reverse_geocode(
        self,
        a_location: Point2D,
        a_record_type: RecordTypeFilter,
        a_status_cb,
        a_data_cb,
    ) -> ApiStatus:
        """Runs a Geocode operation in Search
        Args:
            a_location (Point2D): location to be used in reverse geocoding operation
            a_record_type (RecordTypeFilter): record types to be returned
            a_status_cb ([callable]): status message processor
            a_data_cb ([callable]): data message processor

        Returns:
            ApiStatus: ApiStatus
        """

        message = """{}.ReverseGeocoding {},{{ {} }} """.format(
            self.name, a_location.get_core_str(), str(a_record_type)
        )
        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone, Cor3Status.kErrReverseGeocodingNotPossible]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def update_position(self, a_position: Point2D) -> ApiStatus:
        """Updates the current search location
        Args:
            a_position (Point2D): position

        Returns:
            ApiStatus: ApiStatus
        """
        message = """{}.UpdatePosition {}""".format(
            self.full_name, a_position.get_core_str()
        )
        req = CoreRequest(self._thread)
        req.message = message
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def start_nearby_search(
        self,
        a_search_string: str,
        a_search_filter: SearchFilters,
        a_status_cb,
        a_data_cb,
    ) -> ApiStatus:
        """Run a search nearby - current search position

        Args:
            a_search_string (str): string for search input
            a_search_filter (SearchFilters): Search Filters definition
            a_status_cb ([callable]): status message processor
            a_data_cb ([callable]): data message processor

        Returns:
            ApiStatus: [description]
        """

        message = """{}.StartNearBySearch "{}",{} """.format(
            self.name, a_search_string, str(a_search_filter)
        )
        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def get_poi_details(
        self,
        a_poi_id: str,
        a_status_cb,
        a_data_cb,
    ) -> ApiStatus:
        """Returns all POI details

        Args:
            a_poi_id (str): poi_id
            a_status_cb ([callable]): status message processor
            a_data_cb ([callable]): data message processor

        Returns:
            ApiStatus: [description]
        """

        message = """{}.GetPoiDetails {} """.format(self.name, a_poi_id)
        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(self._process_poi_details_data)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()

        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def _process_poi_details_data(self, a_request: CoreRequest):
        # FIX from result format, parse as an array
        a_request._data = from_string("[{}]".format(a_request.status_content))


    def get_poi_categories_resources(
        self,
        a_status_cb = None,
        a_data_cb = None,
        a_type="MAIN_CATEGORIES",
        a_visibility="VISIBLE",
    ) -> ApiStatus:
        """Returns all POI details

        Args:
            a_poi_id (str): poi_id
            a_status_cb ([callable]): status message processor
            a_data_cb ([callable]): data message processor

        Returns:
            ApiStatus: [description]
        """

        message = """{}.GetPoiCategoryResources {},{}""".format(self.name,
            a_type, a_visibility)
        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()

        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)
