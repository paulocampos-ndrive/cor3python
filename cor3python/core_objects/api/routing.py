from typing import List, Optional

from cor3python.core_objects.api.api_base import ApiBase
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.utils.thread_manager import thread_manager
from cor3python.core_objects.api.api_status import ApiStatus
from cor3python.messaging.core_request import CoreRequest
from cor3python.messaging.status_code import Cor3Status
from cor3python.core_objects.common.routing.profile import Profile
from cor3python.core_objects.common.routing.waypoint import WayPoint
from cor3python.utils.parsing import bool2str

class APIRouting(ApiBase):

    def __init__(self, a_messaging: BaseDispatcher, a_thread=thread_manager.get_thread("ROUTING")):
        super().__init__(a_messaging=a_messaging, a_thread=a_thread)
        self.name = 'Routing'

    def create_profile(self, a_profile: Profile) -> ApiStatus:
        message = r'{}.CreateProfile {}, {{{}}}'.format(self.full_name, a_profile.name, a_profile.create_profile_string())
        req = CoreRequest(self._thread)
        req.message = message
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def destroy_profile(self, a_profile_name: str) -> ApiStatus:
        message = r'{}.{}.Destroy'.format(self.full_name, a_profile_name)
        req = CoreRequest(self._thread)
        req.message = message
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)    

    def create_itinerary(self, a_itinerary_name: str):
        message = "{}.CreateItinerary {}".format(self.full_name, a_itinerary_name)
        req = CoreRequest(self._thread)
        req.message = message
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)    

    def add_graph_cache_profile(self, a_profile_name: str):
        message = "{}.AddGraphCacheProfile {}".format(self.full_name, a_profile_name)
        req = CoreRequest(self._thread)
        req.message = message
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)            

    def remove_graph_cache_profile(self, a_profile_name: str):
        message = "{}.RemoveGraphCacheProfile {}".format(self.full_name, a_profile_name)
        req = CoreRequest(self._thread)
        req.message = message
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)               

    def get_optimal_waypoints_order(self,
                                    a_waypoints_list: List[WayPoint],                                    
                                    a_data_cb, a_status_cb,
                                    a_monitor_as_origin: bool = False,
                                    a_monitor_as_destination: bool = False,
                                    a_lock_first_waypoint: bool = False,
                                    a_lock_last_waypoint: bool = False,
                                    a_use_optimal_algorithm: bool = True,
                                    a_monitor=None):

        monitor_as_origin = bool2str(a_monitor_as_origin)
        monitor_as_destination = bool2str(a_monitor_as_destination)
        use_optimal_algorithm = bool2str(a_use_optimal_algorithm)

        lock_str = ""
        if a_lock_first_waypoint is True:
            lock_str += "lock_first_waypoint:{},".format(
                a_waypoints_list[0].name)
        if a_lock_last_waypoint is True:
            lock_str += "lock_last_waypoint:{},".format(
                a_waypoints_list[-1].name)

        monitor_str = ""
        if a_monitor is not None:
            monitor_str = "monitor:Navigation.{}, ".format(a_monitor.name)

        waypoints_str = ""
        for wp in a_waypoints_list:
            waypoints_str += "{}:{},".format(wp.name,
                                             wp.point.get_core_str())
        if len(waypoints_str) > 0:
            waypoints_str = waypoints_str[:-1]
        waypoints_str = "waypoints:{" + waypoints_str + "}"

        message = "{}.GetOptimalWaypointsOrder {{ {}{},monitor_as_origin:{},monitor_as_destination:{},{}use_optimal_algorithm:{}}}".format(
            self.full_name, monitor_str, waypoints_str, monitor_as_origin,
            monitor_as_destination, lock_str, use_optimal_algorithm)

        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def get_distance_matrix(self,
                            a_origins_list: List[WayPoint], 
                            a_destinations_list: List[WayPoint],                                    
                            a_profile: str,
                            a_data_cb, 
                            a_status_cb,
                            a_fast_calc: bool = False,
                            a_number_workers: int = 2):

        fast_calc = bool2str(a_fast_calc)
        
        # ORIGINS
        origin_str = ""
        for i,wp in enumerate(a_origins_list):
            if i != 0:
                origin_str += ","
            origin_str += "{{ location:{} }}".format(wp.point.get_core_str())        
        origin_str = "origins:[" + origin_str + "]"

        # DESTINATIONS
        destination_str = ""
        for i,wp in enumerate(a_destinations_list):
            if i != 0:
                destination_str += ","
            destination_str += "{{ location:{} }}".format(wp.point.get_core_str())        
        destination_str = "destinations:[" + destination_str + "]"

        message = "{}.GetDistanceMatrix {{ {}, {}, profile:{}, fast_calc:{}, number_of_worker_threads:{} }}".format(
            self.full_name, origin_str, destination_str, a_profile, fast_calc, a_number_workers)

        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)        