from cor3python.core_objects.api.api_base import ApiBase
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.utils.thread_manager import thread_manager
from cor3python.core_objects.api.api_status import ApiStatus
from cor3python.messaging.core_request import CoreRequest
from cor3python.messaging.status_code import Cor3Status


class APITrafficClient(ApiBase):

    def __init__(self, a_messaging: BaseDispatcher, a_thread=thread_manager.get_thread("TRAFFIC_CLIENT")):
        super().__init__(a_messaging=a_messaging, a_thread=a_thread)
        self.name = 'TrafficClient'

    def setup_configuration(self,
                            a_hostname: str,
                            a_port: int,
                            a_config: str,
                            a_client_type: str = ""):

        if a_client_type is not None and a_client_type != "":
            str_client_type = ""","{}" """.format(a_client_type)
        else:
            str_client_type = ""

        message = """{}.SetupConfiguration "{}",{},"{}"{} """.format(self.full_name,
            a_hostname, a_port, a_config, str_client_type)
        req = CoreRequest(self._thread)
        req.message = message
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)
