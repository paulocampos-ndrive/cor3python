from cor3python.core_objects.api.api_base import ApiBase
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.utils.thread_manager import thread_manager
from cor3python.core_objects.api.api_status import ApiStatus
from cor3python.messaging.core_request import CoreRequest
from cor3python.messaging.status_code import Cor3Status


class APIEnvironment(ApiBase):

    def __init__(self, a_messaging: BaseDispatcher, a_thread=thread_manager.get_thread("ENVIRONMENT")):
        super().__init__(a_messaging=a_messaging, a_thread=a_thread)
        self.name = 'Environment'

    def get_system_info(self, a_status_cb = None, a_data_cb = None) -> ApiStatus:
        message = r'{}.GetSystemInformation'.format(self.full_name)
        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def get_versions(self, a_status_cb = None, a_data_cb = None) -> ApiStatus:
        message = r'{}.GetVersions'.format(self.full_name)
        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)

    def get_memory_information(self, a_status_cb = None, a_data_cb = None) -> ApiStatus:
        message = r'{}.GetMemoryInformation'.format(self.full_name)
        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)
