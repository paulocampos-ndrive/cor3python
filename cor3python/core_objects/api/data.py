from typing import Dict, List, Callable, Tuple, Optional

from cor3python.utils.thread_manager import thread_manager
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.messaging.core_request import CoreRequest
from cor3python.messaging.status_code import Cor3Status
from cor3python.core_objects.api.api_base import ApiBase
from cor3python.core_objects.api.api_status import ApiStatus
from cor3python.core_objects.common.types.point_2d import Point2D


class APIData(ApiBase):

    def __init__(self, a_messaging: BaseDispatcher, a_thread=thread_manager.get_thread("DATA")):
        super().__init__(a_messaging=a_messaging, a_thread=a_thread)
        self.name = 'Data'


    def get_edge_basic_info(self, a_locale: str, a_points: List[Point2D], a_status_cb, a_data_cb) -> ApiStatus:
        """Gets edge information from a list of multiple points
        Args:
            a_points (list): List of points
            a_status_cb ([callable]): status message processor
            a_data_cb ([callable]): data message processor

        Returns:
            ApiStatus: ApiStatus
        """
        points_str = ""
        for i, point in enumerate(a_points):
            if i != 0: 
                points_str += ","
            points_str += point.get_core_str()

        message = """{}.GetEdgeBasicInfo {{ locale: "{}", points:[{}] }}""".format(self.full_name, a_locale, points_str)
        req = CoreRequest(self._thread)
        req.message = message
        req.register_data_callback(a_data_cb)
        req.register_status_callback(a_status_cb)
        self.messaging.send(req)
        req.wait()
        if req.core_status in [Cor3Status.kErrNone]:
            return ApiStatus(a_status=True, a_request=req)
        return ApiStatus(a_status=False, a_request=req)
