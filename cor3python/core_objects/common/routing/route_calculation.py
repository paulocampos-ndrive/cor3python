
from typing import Callable, List, Tuple, Optional
from cor3python.utils.thread_manager import WorkerThread
from cor3python.messaging.core_response import CoreResponse
from cor3python.messaging.core_request import CoreRequest
from cor3python.messaging.status_code import Cor3Status
from cor3python.core_objects.common.routing.profile import Profile
from cor3python.core_objects.common.routing.route_calculation_result import RouteCalculationResult

class RouteCalculation:

    def __init__(self) -> None:
        
        self.profile: Profile = None
        self.progress_enable: bool = False
        self.alternatives: int = 0
        self.navigation_monitor = None # TODO: Navigation Monitor
        self.geoavoid = None # TODO: GeoAvoids
        self.calc_timestamp: Optional[int] = None

        # DEBUG AND EXTRA FLAGS
        self.extended_maneuvers: bool = False
        self.extensive_lane_info: bool =  False
        self.export_algorithm_kml:bool = False
        self.export_maneuvers_kml: bool = False
