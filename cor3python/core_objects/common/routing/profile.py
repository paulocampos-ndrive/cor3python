import hashlib
import inspect
import json
import logging

from cor3python.utils.parsing import bool2str, str2bool


class DefaultProfile:
    """Default Profile Settings
    """
    def __init__(self) -> None:
        
        self.mode = "fastest"
        self.toll = True
        self.ferry = True
        self.motorways = True
        self.transportation = "car"
        self.traffic = False

        # Truck + RV
        self.weight = 0.0
        self.width = 0.0
        self.height = 0.0
        self.trailer_length = 0.0
        self.num_axles = 2
        self.num_trailers = 0
        self.hazmat = "None"

        self.residential_roads_routing_class = 5
        self.routing_class_filter = False
        self.residential_roads_filter = False

        # RV
        self.propane_bottle_size = 0.0
        self.num_propane_bottles = 0

        self.unpaved = True
        self.freeways = "Allow"
        self.tunnels = True
        self.country_crossing = True
        self.small_roads = True
        self.uturns = False

        self.undesignated_102in_width_factor = 3.0
        self.undesignated_53ft_trailer_factor = 3.0
        self.undesignated_2trailers_factor = 3.0
        self.undesignated_3trailers_factor = 3.0
        self.residential_road_factor = 500.0
        self.residential_road_max_dist = 1609.0
        self.routing_class_filter_relax_value = 1
        self.small_roads_factor = 4.0
        self.small_roads_routing_class = 3
        self.algorithm = "Preprocessed"  # "Preprocessed or Dynamic

        self.use_turn_costs = False  # Turn costs
        self.turn_costs = "[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]"  # Turn costs

        # more debug parameters
        self.apply_eta_penalties = True
        self.patch_turning_restrictions = True
        self.patch_time_domains = True
        self.create_route_instructions = True
        self.prepare_for_navigation = True
        self.decompress_route_path = True
        self._override_tc = False

    @classmethod
    def attributes(cls):
        x = inspect.getmembers(DefaultProfile(), lambda a:not inspect.isroutine(a))
        return x

    @classmethod
    def attributes_names(cls):
        profile_attributes = [a[0] for a in cls.attributes() if not(a[0].startswith('__') and a[0].endswith('__'))]   
        return profile_attributes

    @classmethod
    def ignore_keys(cls):
        return ['turn_costs', 'ignore_eta_penalties', 'ignore_turning_restrictions']



class Profile(DefaultProfile):

    def __init__(self):
        super().__init__()

    @property
    def name(self):
        return self._hash()

    @property
    def full_name(self):
        return "{}.{}".format("Routing", self.name)

    def _hash(self):
        full_attributes = DefaultProfile.attributes()
        profile_attributes = [a[0] for a in full_attributes if not(a[0].startswith('__') and a[0].endswith('__'))]

        _dict = {}
        for k in profile_attributes:
            _dict[k] = self.__getattribute__(k) 
        return hashlib.sha1(json.dumps(_dict,
                                       sort_keys=True).encode()).hexdigest()                              
  
    def __eq__(self, other: 'Profile') -> bool:
        if other is None:
            return False
        try:
            return self.name == other.name
        except Exception as err:
            logging.warning("Error comparing profiles - {}".format(err))
            return False  

    def load_from_string(self, a_profile_string: str) -> 'Profile':
        key_values = a_profile_string.split(';')
        for key_value in key_values:
            if len(key_value) < 3:
                continue
            key, value = key_value.split('=')

            _type = type(getattr(self, key, ""))
            
            if _type == bool:
                setattr(self, key, str2bool(value))
            else:
                setattr(self, key, _type(value))
        return self

    def export_as_string(self) -> str:
        full_attributes = DefaultProfile.attributes()

        ret_str = ""
        for k in sorted(full_attributes):
            ret_str += "{0}={1};".format(k, getattr(self, k))
        if len(ret_str) > 0:
            ret_str = ret_str[:-1]
        return ret_str

    def create_profile_string(self) -> str:
        message = ""

        _attributes = DefaultProfile.attributes_names()
        for k in sorted(_attributes):
            # handled with specific API
            if k in DefaultProfile.ignore_keys() or k.startswith('_'):
                continue 

            if isinstance(getattr(self, k), bool):
                val = bool2str(getattr(self, k))
            else:
                val = getattr(self, k)
            message += "{0}:{1},".format(k, val)
        
        return message[:-1]

    def export_as_dict(self):
        full_attributes = DefaultProfile.attributes()
        profile_attributes = [a[0] for a in full_attributes if not(a[0].startswith('__') and a[0].endswith('__'))]

        _dict = {}
        for k in profile_attributes:
            _dict[k] = self.__getattribute__(k)         
        
        return _dict