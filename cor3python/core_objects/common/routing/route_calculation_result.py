from typing import Optional, List, Dict


class DictObject:   
    def __init__(self) -> None:
        self._data = {}

    def load(self, a_data):
        self._data = a_data
        if a_data is not None:
            for k in a_data:
                setattr(self, k, a_data[k])
            
    def dict(self):
        return self._data

class CrossesViolates(DictObject):
    def __init__(self, a_data: dict = None):
        super().__init__()

        self.tolls: float = 0
        self.motorways: float = 0
        self.ferries: float = 0
        self.unpaved: float = 0
        self.freeways: float = 0
        self.tunnels: float = 0
        self.smallroads: float = 0
        self.countrycrossing: float = 0
        self.uturns: float = 0
        self.height: float = 0
        self.maxaxlecount: float = 0
        self.width: float = 0
        self.propanesize: float = 0
        self.propanecount: float = 0
        self.norv: float = 0
        self.notruck: float = 0
        self.weight: float = 0
        self.trailerlength: float = 0
        self.numtrailers: float = 0
        self.trailer102: float = 0
        self.hazmat: float = 0
    
        # load data
        self.load(a_data)


class ItineraryRoutes(DictObject):

    def __init__(self, a_data: dict = None):
        super().__init__()

        self.route_distance: float = 0
        self.route_time: float = 0
        self.route_delay_time: float = 0
        self.route_crosses_tolls: bool = False
        self.route_crosses_motorways: bool = False
        self.route_crosses_ferries: bool = False
        self.crosses: CrossesViolates = CrossesViolates()
        self.violates: CrossesViolates = CrossesViolates()
        self.propane_bottle_size_limit: float = 0
        self.rv_trailer_length_limit: float = 0
        self.propane_bottle_count_limit: int = 0
        self.hazmat_permit_required: bool = False
        self.legal_violations: List[str] = list()   

        self.geometry_compressed: str = None

        # load data
        self.load(a_data)
        if 'crosses' in a_data:
            self.crosses = CrossesViolates(a_data['crosses'])
        if 'violates' in a_data:
            self.violates = CrossesViolates(a_data['violates'])

class RouteCalculationResult(DictObject):

    def __init__(self, a_data: dict = None) -> None:
        super().__init__()
        
        self.itinerary_routes: List[ItineraryRoutes] = list()
        self.total_distance: float = 0
        self.total_time: float = 0
        self.total_delay_time: float = 0
        self.crosses_tolls: bool = False
        self.crosses_motorways: bool = False
        self.crosses_ferries: bool = False
        self.num_alternatives: int = 0
        self.num_routes: int = 0
        self.itinerary_name: str = ""
        self.tbt: List[str] = list()
        self.waypoint_index: int = 0
        self.crosses: CrossesViolates = CrossesViolates()
        self.violates: CrossesViolates = CrossesViolates()

        # load data
        self.load(a_data)
        self.itinerary_routes = list()
        for iti_r in a_data['itinerary_routes']:
            self.itinerary_routes.append(ItineraryRoutes(iti_r))
        if 'crosses' in a_data:
            self.crosses = CrossesViolates(a_data['crosses'])
        if 'violates' in a_data:
            self.violates = CrossesViolates(a_data['violates'])