import hashlib
import json

from cor3python.core_objects.common.types.point_2d import Point2D


class WayPoint(object):

    def __init__(self, a_point: Point2D, a_search_id: str = None):
        self.point = a_point
        self.search_id = a_search_id

    def description(self, a_formated: bool = False):
        if not a_formated:
            return "{0}".format(str(self.point))
        else:
            return "{0}".format(str(self.point))
            
    @property
    def name(self):
        return "POINT_" + hashlib.sha1(
            json.dumps(self.point.get_as_dict(), sort_keys=True).encode()).hexdigest()
