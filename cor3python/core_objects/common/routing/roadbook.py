import logging
from cor3python.messaging.core_response import CoreResponse


class RoadbookEntry(object):


    def __init__(self, a_response_data: dict, a_raw_str: str):
        self.id = None
        self.name = None
        self.street_name = None
        self.street_names = None
        self.distance = None
        self.time = None
        self.traffic_delay = None
        self.traffic_distance = None
        self.traffic_severity = None
        self.motorway = None
        self.toll = None
        self.ferry = None
        self.description = None
        self.turn_number = None
        self.exit_number = None
        self.towards = None
        self.signposts_active = None
        self.signposts_inactive = None
        self.lane_info = None
        self.text = None

        self.raw_str = a_raw_str

        for k in a_response_data:
            self.add(k, a_response_data[k])

    def add(self, k, v):
        setattr(self, k, v)

    def __eq__(self, other):
        if self.id != other.id:
            return False
        if self.name != other.name:
            return False
        if self.distance != other.distance:
            return False
        if self.turn_number != other.turn_number:
            return False
        if self.exit_number != other.exit_number:
            return False
        return True


    @staticmethod
    def get_instruction_side(a_instruction):
        for test in ['Right', 'ExitR', 'GoR']:
            if test in a_instruction:
                return False
        return True



class Roadbook(object):
    def __init__(self):
        self._roadbook = list()
        self._roadbook_dict = list()
    
    def clear(self):
        self._roadbook = list()
        self._roadbook_dict = list()

    def add_entry(self, a_response: CoreResponse):
        self._roadbook.append(RoadbookEntry(a_response_data=a_response.data, a_raw_str=a_response.content))
    
    def get_roadbook(self):
        return self._roadbook

    def get_roadbook_dict(self):
        return self._roadbook_dict
