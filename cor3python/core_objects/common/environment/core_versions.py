from cor3python.messaging.core_response import CoreResponse
from cor3python.messaging.core_request import CoreRequest


class CoreVersions():

    def __init__(self):
        self.core = None
        self.all_files = None


    def load(self, a_core_versions:dict):
        """Loads versions return by cor3API

        Args:
            a_core_versions (dict): response from cor3 API

        Returns:
            ProductInfo: self
        """
        for k in a_core_versions:
            if hasattr(self,k):
                setattr(self,k,a_core_versions[k])

        return self

    def process_data_response(self, a_core_response: CoreResponse):
        self.load(a_core_response.data)
