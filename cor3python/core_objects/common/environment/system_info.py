from cor3python.messaging.core_response import CoreResponse
from cor3python.messaging.core_request import CoreRequest


class SystemInfo():

    def __init__(self):
        self.total_cpus = None
        self.system_memory = None
        self.core_safe_memory = None
        self.core_max_memory = None
        self.hostname = None
        self.allocator_type = None

    def load(self, a_system_info:dict):
        """Loads info from product return by cor3API

        Args:
            a_system_info (dict): response from cor3 API

        Returns:
            ProductInfo: self
        """
        for k in a_system_info:
            if hasattr(self,k):
                setattr(self,k,a_system_info[k])

        return self


    def process_data_response(self, a_core_response: CoreResponse):
        self.load(a_core_response.data)
