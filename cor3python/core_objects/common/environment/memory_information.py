from cor3python.messaging.core_response import CoreResponse
from cor3python.messaging.core_request import CoreRequest


class MemoryInformation():

    def __init__(self):
        self.global_allocated_memory_size = None
        self.generic_allocated_memory_size = None
        self.graphics_allocated_memory_size = None
        self.page_count = None
        self.free_page_count = None
        self.routing_calculation_count = None
        self.cache_current_size = None
        self.cache_max_size = None

    def load(self, a_memory_information:dict):
        for k in a_memory_information:
            if hasattr(self,k):
                setattr(self,k,a_memory_information[k])

        return self

    def process_data_response(self, a_core_response: CoreResponse):
        self.load(a_core_response.data)
