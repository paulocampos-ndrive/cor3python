import logging

class Point2D:
    DIGITS = 6

    def __init__(self, a_lat: float = None, a_lng:float = None, a_type:str = 'C'):
        self._lat = None
        self._lon = None
        self._type = None

        if a_lat is not None:
            self._lat = a_lat
        if a_lng is not None:
            self._lon = a_lng
        if a_type is not None:
            self._type = a_type

    @property
    def lat(self):
        return self._lat

    @property
    def lng(self):
        return self._lon

    def load_core_list(self, a_list:list) -> object:
        """Load a core list with lat, lon

        Args:
            a_list (list): [lat,lon,type]
        Return:
            Point2D instance    
        """
        if len(a_list) != 3:
            logging.error("Invalid Point - {}".format(a_list))
            return self
        self._lon = round(float(a_list[1]), Point2D.DIGITS)
        self._lat = round(float(a_list[2]), Point2D.DIGITS)
        self._type = str(a_list[0])
        return self

    def get_core_str(self) -> str:
        """Returns Point2D in Core string format"""
        return "[{},{:.{width}f},{:.{width}f}]".format(
            self._type, self._lon, self._lat, width=Point2D.DIGITS)

    def get_as_dict(self) -> dict:
        return {'lat': self._lat, 'lon': self._lon, 'type': self._type}
