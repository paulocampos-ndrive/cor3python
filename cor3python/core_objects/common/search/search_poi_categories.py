from anytree import Node, NodeMixin, RenderTree, AsciiStyle, find, PreOrderIter

from cor3python.messaging.core_response import CoreResponse
from cor3python.messaging.core_request import CoreRequest


class PoiCategory(NodeMixin):
    
    def __init__(self, a_poi_cat_info: list=[], parent=None, children=None): 
        super(PoiCategory, self).__init__()

        self.id = None
        self.parentid = None
        self.name = None
        self.display_class = None
        """
        Id: Category identifier.
        ParentId:
        Name: Category name
        Have_childs: Indicates if category have childs.
        Have_visible_childs: Indicates if category have visible childs.
        Is_main_category: Indicates if category is main category (Don't have parent).
        Display_class: Category display class.
        """ 
        if len(a_poi_cat_info) > 0:
            self.id = a_poi_cat_info[2]
            self.parentid = a_poi_cat_info[3]
            self.name = a_poi_cat_info[4]
            self.display_class = int(a_poi_cat_info[8])
        else:
            self.name = "ROOT"

        self.parent = parent
        if children:
            self.children = children


class PoiCategories(object):
    def __init__(self, locale: str):
        self.locale = locale
        self._nodes = dict()
        self._initialized = False

    def process_data_response(self, a_core_response: CoreResponse):
        poi_cat = PoiCategory(a_core_response.data)

        # Tree Root
        if len(self._nodes) == 0:
            self._nodes['ROOT'] = poi_cat
            return

        # Tree branches
        self._nodes[poi_cat.id] = poi_cat
        if poi_cat.parentid in self._nodes and poi_cat.parentid != poi_cat.id:
            poi_cat.parent = self._nodes[poi_cat.parentid]
        else:
            poi_cat.parent = self._nodes['ROOT']

    def process_status_request(self, a_core_request: CoreRequest):
        if len(self._nodes) > 1:
            self._initialized = True

    def print_tree(self):
        if self._initialized:
            print(RenderTree(self._nodes['ROOT'], style=AsciiStyle()).by_attr())

    def find(self, category_id) -> PoiCategory:
        if not self._initialized:
            return None
        return find(self._nodes['ROOT'], lambda node: node.id == category_id)

    def iter_nodes(self):
        if not self._initialized:
            return []
        return [node for node in PreOrderIter(self._nodes['ROOT'])][1:]

    def __len__(self):
        if not self._initialized:
            return 0
        return len(self._nodes)-1

    def root(self):
        if not self._initialized:
            return None
        return self._nodes['ROOT']