from enum import IntEnum
from typing import Callable

from cor3python.utils.thread_manager import thread_manager, WorkerThread
from cor3python.core_objects.common.search.search_result import SearchResult


class SearchUpdateEvent:
    """Object used to report a search progress event 
    """
    class UpdateType(IntEnum):
        ADD = 0
        DELETE = 1
        UPDATE = 2

    def __init__(self, a_type: UpdateType, a_data: SearchResult):
        """
        Args:
            a_type (UpdateType): Type of the event SearchUpdateEvent.UpdateType
            a_data (SearchResult): SearchResult object involved in the update
        """
        self._type = a_type
        self._data = a_data
    
    @property
    def type(self) -> UpdateType:
        return self._type

    @property
    def data(self) -> SearchResult:
        return self._data


class SearchUpdateHandler:
    """
    Update handler to be used in search update events
    """
    def __init__(self, a_thread:WorkerThread, a_cb_method:  Callable[[SearchUpdateEvent], None]):
        """
        Args:
            a_thread (WorkerThread): Thread to be used in the notification
            a_cb_method (Callable[[SearchUpdateEvent], None]): method to be called, receiving the SearchUpdateEvent
        """
        self._thread = a_thread
        self._callback = a_cb_method
    
    def run(self, a_search_update: SearchUpdateEvent):
        self._thread.run(self._callback, a_search_update)
