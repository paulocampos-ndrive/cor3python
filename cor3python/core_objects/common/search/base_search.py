import logging
from cor3python.messaging.core_request import CoreRequest
from cor3python.messaging.core_response import CoreResponse
from cor3python.core_objects.api.api_status import ApiStatus
from cor3python.messaging.status_code import Cor3Status
from cor3python.core_objects.common.search.search_result import SearchResult
from cor3python.utils.parsing import str2bool
from cor3python.core_objects.common.types.point_2d import Point2D
from cor3python.core_objects.common.search.update_handler import SearchUpdateHandler, SearchUpdateEvent


class BaseSearch:
    ADD = 'Add'
    DELETE = 'Delete'
    UPDATE = 'Update'

    def __init__(self, a_output_format: dict):
        self._output_format = a_output_format

        self._api_status = None
        # dict for results processing(id -> data)
        self._results = dict()

        # list for final ordered results
        self._ordered_results = list()

        # update listeners
        self._listeners = list()
    
    @property
    def api_status(self):
        if self._api_status:
            return self._api_status
        else:
            return ApiStatus(False, None, "Unknown error")

    @property
    def results(self):
        return self._ordered_results

    def add_listener(self, a_update_handler:SearchUpdateHandler):
        self._listeners.append(a_update_handler)
    
    def notify_listeners(self, a_type: SearchUpdateEvent.UpdateType, a_result: SearchResult):
        event = SearchUpdateEvent(a_type, a_result)
        for listener in self._listeners:
            listener.run(event)

    def data_message(self, a_response: CoreResponse):
        _type = None
        _result = None
        action = a_response.data[1]
        
        if action == BaseSearch.ADD:
            _type = SearchUpdateEvent.UpdateType.ADD
            result_type = a_response.data[0]
            result_id = a_response.data[2]
            data = a_response.data[3:]
            _result = self._add_result(result_type, result_id, data)

        elif action == BaseSearch.DELETE:
            _type = SearchUpdateEvent.UpdateType.DELETE
            result_id = a_response.data[2]
            _result = self._remove_result(result_id)

        elif action == BaseSearch.UPDATE:
            _type = SearchUpdateEvent.UpdateType.UPDATE
            result_id = a_response.data[2]
            new_rank = a_response.data[3]
            _result = self._update_result(result_id, new_rank)
        
        # Notify listeners for search update
        self.notify_listeners(_type, _result)

    def status_message(self, a_request: CoreRequest):
        if a_request.core_status != Cor3Status.kErrNone:
            self._api_status = ApiStatus(False, a_request)
            return
        self._api_status = ApiStatus(True, a_request)            
        self._ordered_results = sorted(list(self._results.values()))

    def _convert_to_output_format(self, a_result_type: str, a_result_data: list) -> dict:
        named_dict = {}
        for i, col in enumerate(self._output_format[a_result_type]):

            if isinstance(col.output_type, bool):                
                named_dict[col.__name__] = str2bool(a_result_data[i])
            elif isinstance(col.output_type, str):                
                named_dict[col.__name__] = str(a_result_data[i])
            elif isinstance(col.output_type, int):                
                named_dict[col.__name__] = int(a_result_data[i])  
            elif isinstance(col.output_type, float):                
                named_dict[col.__name__] = float(a_result_data[i])   
            elif isinstance(col.output_type, Point2D):                
                named_dict[col.__name__] = Point2D().load_core_list(a_result_data[i])                                                                
            else:
                named_dict[col.__name__] = a_result_data[i]
        return named_dict

    def _add_result(self, a_result_type: str, a_result_id: str, a_result_data: list):
        named_dict = self._convert_to_output_format(a_result_type, a_result_data)
        result = SearchResult(a_result_id, a_result_type, named_dict)
        self._results[result.id] = result
        return result

    def _update_result(self, a_result_id: str, a_new_rank: int):
        try:
            self._results[a_result_id].rank = a_new_rank
            return self._results[a_result_id]
        except Exception as err:
            logging.error("Error updating rank({}) for result({}) - {}".format(a_new_rank, a_result_id, err))

    def _remove_result(self, a_result_id):
        try:
            result = self._results[a_result_id]
            del self._results[a_result_id]
            return result
        except Exception as err:
            logging.error("Error removing result({}) - {}".format(a_result_id, err))
