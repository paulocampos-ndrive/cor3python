
class SearchResult:
    def __init__(self, a_result_id: str, a_result_type: str, a_result_dict: dict):
        self._current_idx = 0
        self._properties_names = list()

        # define result id
        setattr(self,'id', a_result_id)

        # define result type
        setattr(self,'type', a_result_type)        

        for k in a_result_dict:
            setattr(self, k, a_result_dict[k])
            self._properties_names.append(k)
        self._result_dict = a_result_dict

    # compare criteria is RANK
    def __lt__(self, other):
        my_value = 9999999999999999999
        other_value = 9999999999999999999
        if hasattr(self, 'rank'):
            my_value = self.rank
        if hasattr(other, 'rank'):
            other_value = other.rank
        return my_value < other_value

    # Output string for each type
    def __str__(self):
        a_string = ""
        if self.type == 'C':
            return self._format_c()
        if self.type == 'S':
            return self._format_s()
        if self.type in ['R', 'X']:
            return self._format_r()
        if self.type == 'P':
            return self._format_p()
        if self.type == 'A':
            return self._format_a()
        if self.type == 'L':
            return self._format_l()
        if self.type == 'H':
            return self._format_h()
        if self.type == 'Z':
            return self._format_z()
        if self.type == 'W':
            return self._format_w()

        for attr, value in self:
            a_string += "{0}: {1}, ".format(attr, value)
        a_string = a_string[:-2]
        return a_string

    def __next__(self):
        if self._current_idx < len(self._properties_names):
            self._current_idx += 1
            k = sorted(self._properties_names)[self._current_idx - 1]
            return k, getattr(self, k)
        else:
            self._current_idx = 0
            raise StopIteration

    def __iter__(self):
        return self

    def text(self):
        ret_str = ""
        for k in sorted(self._result_dict):
            ret_str += "{0}: {1}\n".format(k, self._result_dict[k])
        return ret_str

    def _format_h(self):
        return "{0}\n{1}".format(self.formatted_street,
                                 self.formatted_area_address_postalcode)

    def _format_w(self):
        return "{0}\n{1}".format(self.point, self.formatted_address)

    def _format_r(self):
        return "{0}".format(self.formatted_address)

    def _format_p(self):
        return "{0}\n{1}".format(self.primary_name, self.formatted_address)

    def _format_a(self):
        return "{0}\n{1}".format(self.primary_name,
                                 self.formatted_area_address)

    def _format_l(self):
        return "{0}\n{1}".format(self.primary_name,
                                 self.formatted_area_address)

    def _format_c(self):
        return "{0}".format(self.primary_name)

    def _format_s(self):
        return "{0}\n{1}".format(self.primary_name,
                                 self.formatted_area_address)

    def _format_z(self):
        return "{0}\n{1}".format(self.primary_name,
                                 self.formatted_area_address)

   
