from cor3python.core_objects.common.types.point_2d import Point2D

class SearchOutputTypes:

    class basic_type:
        def __init__(self):
            pass

    class primary_name(basic_type):
        output_type = str()

    class language_code(basic_type):
        output_type = int()

    class abbreviation(basic_type):
        output_type = str()

    class have_states(basic_type):
        output_type = bool()

    class have_postalcodes(basic_type):
        output_type = bool()

    class have_house_numbers(basic_type):
        output_type = bool()

    class rank(basic_type):
        output_type = int()

    class details(basic_type):
        output_type = dict()

    class area_address(basic_type):
        output_type = list()

    class postal_name(basic_type):
        output_type = str()

    class formatted_address(basic_type):
        output_type = str()

    class formatted_address_without_postalcode_and_country(basic_type):
        output_type = str()

    class formatted_area_address(basic_type):
        output_type = str()

    class formatted_street(basic_type):
        output_type = str()

    class formatted_area_address_postalcode(basic_type):
        output_type = str()

    class distance(basic_type):
        output_type = float()

    class point(basic_type):
        output_type = Point2D()

    class postal_code(basic_type):
        output_type = str()

    class area_id(basic_type):
        output_type = str()

    class entry_point(basic_type):
        output_type = Point2D()

    class match_type(basic_type):
        output_type = str()

    class category_id(basic_type):
        output_type = str()

    class edge_traffic_info(basic_type):
        output_type = list()

    class address_short_description(basic_type):
        output_type = str()

