from typing import Optional
from cor3python.utils.parsing import bool2str


class RecordTypeFilter:
    def __init__(self, a_record_types: list = None):
        self._record_types: set = set()
        if a_record_types:
            self.add_record_types(a_record_types)

    def add_record_types(self, a_record_types: list):
        for rec in a_record_types:
            self._record_types.add(rec)

    def add_record_type(self, a_record_type: str):
        self._record_types.add(a_record_type)

    def _build(self):
        return "record_type: " + "".join(self._record_types)

    def __str__(self):
        return self._build()


class FullSearchConfig:
    def __init__(self, a_name: str) -> None:
        self.name = a_name

        self.search_enabled: bool = None
        self.search_full_words: bool = None
        self.search_partial_words: bool = None
        self.stop_on_results_found: bool = None

    def build(self) -> str:
        flag = False
        _str = "{}:{{".format(self.name)
        
        if self.search_enabled is not None:
            flag = True
            _str += "search_enabled:{},".format(bool2str(self.search_enabled))
        if self.search_full_words is not None:
            flag = True
            _str += "search_full_words:{},".format(bool2str(self.search_full_words))
        if self.search_partial_words is not None:
            flag = True
            _str += "search_partial_words:{},".format(bool2str(self.search_partial_words))
        if self.stop_on_results_found is not None:
            flag = True
            _str += "stop_on_results_found:{},".format(bool2str(self.stop_on_results_found))                        
        if flag:
            return _str[:-1] + "},"
        return ""


class SearchFilters:
    def __init__(
        self, a_record_types: list = None, a_ids: list = None, a_max_distance=None
    ):
        """[summary]

        Args:
            a_record_types (list[pair], optional): a list of pair with record_types -> count
            a_ids (list, optional): [description]. a list of id to filter
        """
        self._ids = list()
        self._record_types = list()
        self._max_distance = None

        # Full search config keys
        self.search_nearby_category_name = FullSearchConfig("search_nearby_category_name")
        self.search_nearby_features = FullSearchConfig("search_nearby_features")
        self.search_formatted_query = FullSearchConfig("search_formatted_query")
        self.search_not_formatted_query = FullSearchConfig("search_not_formatted_query")
        self.search_nearby_features_expand = FullSearchConfig("search_nearby_features_expand")

        if a_record_types:
            self._record_types.extend(a_record_types)

        if a_ids:
            self._ids.extend(a_ids)

        if a_max_distance is not None:
            self._max_distance = a_max_distance

    def add_id(self, a_id: str):
        if a_id:
            self._ids.append(a_id)

    def add_record_types(self, a_record_types: list):
        self._record_types.append(a_record_types)

    def _build(self) -> str:
        """example: {record_types:[[R,40]], ids:[A81I44598]}

        Returns:
            [str]: String format for cor3Api
        """
        _str = ""

        if len(self._record_types) > 0:
            _str += "record_types:["
            for i, rec_type in enumerate(self._record_types):
                if i > 0:
                    _str += ","
                _str += "[{},{}]".format(rec_type[0], rec_type[1])
            _str += "]"

        if len(_str) > 0 and len(self._ids) > 0:
            _str += ", "

        if len(self._ids) > 0:
            _str += "ids:["
            for i, _id in enumerate(self._ids):
                if i > 0:
                    _str += ","
                _str += "{}".format(_id)
            _str += "]"

        if self._max_distance is not None:
            _str += ", max_distance: {}".format(self._max_distance)

        # full search config
        full_search_config = self.search_nearby_category_name.build()
        full_search_config += self.search_nearby_features.build()
        full_search_config += self.search_nearby_features_expand.build()
        full_search_config += self.search_not_formatted_query.build()
        full_search_config += self.search_formatted_query.build()
        if len(full_search_config) > 0:
            _str += ",full_search_config:{" + full_search_config[:-1] + "}"
    
        # FINAL FILTERS DICT
        _str = "{" + _str + "}"
        return _str

    def __str__(self):
        return self._build()
