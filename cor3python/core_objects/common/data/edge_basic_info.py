from typing import Dict, List, Callable, Tuple, Optional

from cor3python.core_objects.common.types.point_2d import Point2D
from cor3python.utils.parsing import str2bool

class EdgeName:
    def __init__(self, a_data: dict) -> None:
        self.locale: Optional[str] = None
        self.name: Optional[str] = None

        self.load(a_data)
    
    def load(self, a_data: dict) -> 'EdgeName':
        if 'name' in a_data:
            self.name = a_data['name']
        if 'locale' in a_data:
            self.locale = a_data['locale']
        return self

class EdgeAccess:
    def __init__(self, a_data: dict) -> None:
        self.automobiles: Optional[bool] = None
        self.pedestrians: Optional[bool] = None
        self.buses: Optional[bool] = None
        self.taxis: Optional[bool] = None
        self.car_pools: Optional[bool] = None
        self.trucks: Optional[bool] = None
        self.deliveries: Optional[bool] = None
        self.emergency_vehicles: Optional[bool] = None
        self.motorcycles: Optional[bool] = None

        self.load(a_data)
    
    def load(self, a_data: dict) -> 'EdgeAccess':
        if 'automobiles' in a_data:
            self.automobiles = str2bool(a_data['automobiles'])
        if 'pedestrians' in a_data:
            self.pedestrians = str2bool(a_data['pedestrians'])
        if 'buses' in a_data:
            self.buses = str2bool(a_data['buses'])
        if 'taxis' in a_data:
            self.taxis = str2bool(a_data['taxis'])
        if 'car_pools' in a_data:
            self.car_pools = str2bool(a_data['car_pools'])
        if 'trucks' in a_data:
            self.trucks = str2bool(a_data['trucks'])
        if 'deliveries' in a_data:
            self.deliveries = str2bool(a_data['deliveries'])
        if 'emergency_vehicles' in a_data:
            self.emergency_vehicles = str2bool(a_data['emergency_vehicles'])
        if 'motorcycles' in a_data:
            self.motorcycles = str2bool(a_data['motorcycles'])                                                                                                
        return self        


class EdgeAttributes:
    def __init__(self, a_data: dict) -> None:
        self.tollway: Optional[bool] = None
        self.bridge: Optional[bool] = None
        self.tunnel: Optional[bool] = None
        self.boat_ferry: Optional[bool] = None
        self.rail_ferry: Optional[bool] = None
        self.private_road: Optional[bool] = None
        self.freeway: Optional[bool] = None
        self.unpaved: Optional[bool] = None
        self.physical_class: Optional[str] = None

        self.load(a_data)

    def load(self, a_data: dict) -> 'EdgeAccess':
        if 'tollway' in a_data:
            self.tollway = str2bool(a_data['tollway'])
        if 'bridge' in a_data:
            self.bridge = str2bool(a_data['bridge'])
        if 'tunnel' in a_data:
            self.tunnel = str2bool(a_data['tunnel'])
        if 'boat_ferry' in a_data:
            self.boat_ferry = str2bool(a_data['boat_ferry'])
        if 'rail_ferry' in a_data:
            self.rail_ferry = str2bool(a_data['rail_ferry'])
        if 'private_road' in a_data:
            self.private_road = str2bool(a_data['private_road'])
        if 'freeway' in a_data:
            self.freeway = str2bool(a_data['freeway'])
        if 'unpaved' in a_data:
            self.unpaved = str2bool(a_data['unpaved'])
        if 'physical_class' in a_data:
            self.physical_class = str(a_data['physical_class'])                                                                                                
        return self        


class EdgeBasicInfo:

    def __init__(self, a_data: dict) -> None:
        self.edge_id: Optional[str] = None
        self.snap_point: Optional[Point2D] = None
        self.snap_distance: Optional[float] = None
        self.address: Optional[str] = None

        self.names: Optional[List[EdgeName]] = list()
        self.access: Optional[EdgeAccess] = None
        self.attributes: Optional[EdgeAttributes] = None

        self.load(a_data)

    def load(self, a_data: dict) -> 'EdgeAccess':
        if len(a_data) == 0:
            return None

        if 'edge_id' in a_data:
            self.edge_id = a_data['edge_id']
        if 'snap_point' in a_data:
            self.snap_point = a_data['snap_point']
        if 'snap_distance' in a_data:
            self.snap_distance = a_data['snap_distance']
        if 'address' in a_data:
            self.address = a_data['address']
        if 'names' in a_data:
            for name_dict in a_data['names']:
                self.names.append(EdgeName(name_dict))
        if 'access' in a_data:
            self.access = EdgeAccess(a_data['access'])
        if 'attributes' in a_data:
            self.attributes = EdgeAttributes(a_data['attributes'])            

        return self    