

class ProductInfo():

    def __init__(self):
        self.filename = None
        self.sku = None
        self.crc43 = None
        self.size = None

        # Plist
        self.country_code = None
        self.data_provider = None
        self.data_version = None
        self.major = None
        self.map_sku = None
        self.map_tool_version = None
        self.minor = None
        self.product_type = None
        self.search_timestamp = None
        self.state_identifier = None
        self.dataset_identifiers = None
        self.dataset_neighbours = None
        self.driving_side = None
        self.gpu_extension = None
        self.traffic_type = None
        self.transform_name = None
        self.locale = None
        self.version = None
        self.url = None
        self.audio_format = None
        self.gender = None
        self.sample_rate = None
        self.type = None
        self.blocked = None

    def load(self, a_product_info:dict):
        """Loads info from product return by cor3API

        Args:
            a_product_info (dict): response from cor3 API

        Returns:
            ProductInfo: self
        """
        for k in a_product_info:
            if hasattr(self,k):
                setattr(self,k,a_product_info[k])
        if 'plist' in a_product_info:
            for k in a_product_info['plist']:
                if hasattr(self,k):
                    setattr(self,k,a_product_info['plist'][k])
        return self
