from threading import Lock
from typing import Dict, List, Callable, Tuple, Optional

from cor3python.messaging.core_response import CoreResponse
from cor3python.messaging.core_request import CoreRequest
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.utils.thread_manager import thread_manager, WorkerThread
from cor3python.utils.log import log_api_error
from cor3python.messaging.status_code import Cor3Status
from cor3python.core_objects.api.api_status import ApiStatus
from cor3python.core_objects.base import Base
from cor3python.core_objects.api.itinerary import APIItinerary
from cor3python.core_objects.common.routing.waypoint import WayPoint
from cor3python.core_objects.common.routing.profile import Profile
from cor3python.core_objects.common.routing.route_calculation import RouteCalculation
from cor3python.core_objects.common.routing.route_calculation_result import RouteCalculationResult
from cor3python.core_objects.common.routing.roadbook import Roadbook


class Itinerary(Base):

    def __init__(self, a_messaging: BaseDispatcher, a_itinerary_name: str, a_parent: str = "Routing"):
        super().__init__()

        self.name = a_itinerary_name

        # Create API Level object
        self._api = APIItinerary(
            a_messaging, a_itinerary_name=a_itinerary_name, a_parent=a_parent)

        # Waypoints
        self._waypoints = list()

        # Progress Listeners
        self._progress_listeners: List[Tuple[WorkerThread, Callable]] = list()

        # Calculation progress
        self._progress_pct: int = 0
        self._itinerary_route_finder_progress: int = 0
        self._itinerary_route_instructions_progress: int = 0
        self._itinerary_route_direction_fix_progress: int = 0

        # Roadbook
        self._roadbook : Roadbook = Roadbook()

        # Itinerary Calculation Result
        self._calc_finished = False
        self._calc_result: RouteCalculationResult = None
        self._calc_geometry_compressed: List[str] = list()

    @property
    def calculation_result(self):
        if self._calc_finished:
            return self._calc_result
        return RouteCalculationResult()

    @property
    def calculation_finished(self):
        return self._calc_finished

    @property
    def roadbook(self):
        return self._roadbook.get_roadbook()

    def _clear(self):
        self._waypoints = list()
        self._calc_finished = False
        self._calc_result = None
        self._calc_geometry_compressed = list()

        # reset progress
        self._progress_pct = 0
        self._itinerary_route_finder_progress = 0
        self._itinerary_route_instructions_progress = 0
        self._itinerary_route_direction_fix_progress = 0

    def process_calc_data_response(self, a_core_response: CoreResponse):
        """Handle data messages from calculation

        Args:
            a_core_response (CoreResponse)
        """
        data = a_core_response.data
        if 'itinerary_route_finder_progress' in data:
            self._itinerary_route_finder_progress = data[
                'itinerary_route_finder_progress']
        if 'itinerary_route_instructions_progress' in data:
            self._itinerary_route_instructions_progress = data[
                'itinerary_route_instructions_progress']
        if 'itinerary_route_direction_fix_progress' in data:
            self._itinerary_route_direction_fix_progress = data[
                'itinerary_route_direction_fix_progress']

        self._progress_pct = max([self._itinerary_route_finder_progress,
                                 self._itinerary_route_instructions_progress, self._itinerary_route_direction_fix_progress])

        # notify listeners about current route calculation pct
        for listener in self._progress_listeners:
            listener[0].run(listener[1], self._progress_pct)

    def process_calc_status_message(self, a_request: CoreRequest):
        """Handle status from calculation, route details

        Args:
            a_request (CoreRequest)
        """
        if a_request.core_status in [Cor3Status.kErrNone]:
            self._calc_result = RouteCalculationResult(a_request.data)

        self._calc_finished = True

    def register_progress_listener(self, a_thread: WorkerThread, a_callback: Callable):
        self._progress_listeners.append((a_thread, a_callback))

    def process_export_geometry_data_response(self, a_core_response: CoreResponse):
        """Handle data messages from export_geometry

        Args:
            a_core_response (CoreResponse)
        """
        data = a_core_response.data
        for i, route in enumerate(data):
            if 'compressed_line' in route:
                self._calc_result.itinerary_routes[i].geometry_compressed = route['compressed_line']

    def add_waypoint(self, a_waypoint: WayPoint) -> bool:
        """Add a Waypoint to the itinerary

        Args:
            a_waypoint (WayPoint): Waypoint

        Returns:
            bool: Success
        """

        api_result = self._api.add_waypoint(a_waypoint)

        if not api_result.status:
            log_api_error(api_result)
            return False

        self._waypoints.append(a_waypoint)
        return True

    def clear(self) -> bool:
        """Clear current Itinerary

        Returns:
            bool: Success
        """

        api_result = self._api.clear()

        if not api_result.status:
            log_api_error(api_result)
            return False

        # clear internally
        self._clear()
        return True
  

    def calculate(self, a_route_calculation: RouteCalculation, a_timeout: int = None) -> ApiStatus:
        """Runs a route calculation

        Args:
            a_route_calculation (RouteCalculation): Route calculation instance, with parameters and results

        Returns:
            ApiStatus: success
        """
        api_result = self._api.calculate(a_route_calculation=a_route_calculation,
                                         a_timeout=a_timeout,
                                         a_data_cb=self.process_calc_data_response,
                                         a_status_cb=self.process_calc_status_message)

        if not api_result.status:
            log_api_error(api_result)
            return api_result

        return api_result

    def export_itinerary(self, a_simplify_meters: int = 1):
        """Export itinerary geometry

        Args:
            a_simplify_meters (int): Simplification factor

        Returns:
            bool: success
        """
        api_result = self._api.export_itinerary(a_simplify_meters=a_simplify_meters,
                                                a_data_cb=self.process_export_geometry_data_response)

        if not api_result.status:
            log_api_error(api_result)
            return api_result

        return api_result

    def init_from_itinerary_alternative(self, a_main_itinerary: 'Itinerary', a_alternative: int) -> bool:
        """Gets an alternative from an existing route calculation

        Args:
            a_main_itinerary (Itinerary): Original route calculation
            a_alternative (int): alternative number

        Returns:
            ApiStatus: success
        """
        api_result = self._api.init_from_itinerary_alternative(
            a_main_itinerary.api.full_name, a_alternative, self.process_calc_data_response, self.process_calc_status_message)

        if not api_result.status:
            log_api_error(api_result)
            return api_result

    def get_smart_roadbook(self, a_first_route: int = 0, a_last_route: int = None, a_lane_info: bool = False, a_monitor: str = None,
                           a_warnings: Optional[List[str]] = None, a_traffic_minimum_severity: int = 0, a_locale: str = None, a_mode: str = "ToManeuver"):

        if a_last_route is not None:
            last_route_index = a_last_route
        else:
            last_route_index = self._calc_result.num_routes

        # clear any previous readbook
        self._roadbook.clear()

        api_result = self._api.get_smart_roadbook(a_first_route, a_last_route=last_route_index,
                                                  a_data_cb=self.process_roadbook_data_response,
                                                  a_status_cb=self.process_roadbook_status_response,
                                                  a_mode=a_mode,
                                                  a_lane_info=a_lane_info, 
                                                  a_traffic_minimum_severity=a_traffic_minimum_severity,
                                                  a_monitor=a_monitor,
                                                  a_locale=a_locale,
                                                  a_warnings=a_warnings)
        
        if not api_result.status:
            log_api_error(api_result)
            return api_result 

    def process_roadbook_data_response(self, a_core_response: CoreResponse):
        self._roadbook.add_entry(a_core_response)

    def process_roadbook_status_response(self, a_request: CoreRequest):
        pass
