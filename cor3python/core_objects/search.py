from threading import Lock
from typing import Dict

from cor3python.core_objects.common.types.point_2d import Point2D
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.utils.thread_manager import thread_manager
from cor3python.utils.log import log_api_error
from cor3python.core_objects.base import Base
from cor3python.core_objects.api.search import APISearch
from cor3python.core_objects.common.search.search_filters import (
    SearchFilters,
    RecordTypeFilter,
)
from cor3python.core_objects.common.search.base_search import BaseSearch
from cor3python.core_objects.common.search.search_output_types import SearchOutputTypes
from cor3python.core_objects.common.search.update_handler import SearchUpdateHandler
from cor3python.core_objects.common.search.search_poi_categories import PoiCategories


class Search(Base):
    # Global cache for poi categories
    _poi_categories: Dict[str, PoiCategories] = dict()
    _poi_categories_lock = Lock()

    def __init__(self, a_messaging: BaseDispatcher, a_name: str = None):
        super().__init__()

        # Create API Level object
        self._api = APISearch(a_messaging, a_name)

        # Search name
        self.name = self._api.full_name

        # properties
        self._locale = None
        self._position = None
        self._properties["full_search_nearby_search"] = None

        # output format
        self._output_format = None

        # Load defaults
        self._default_search_config()

    def _default_output_format(self):
        _format = dict()

        _format["C"] = [
            SearchOutputTypes.distance,
            SearchOutputTypes.primary_name,
            SearchOutputTypes.language_code,
            SearchOutputTypes.abbreviation,
            SearchOutputTypes.have_states,
            SearchOutputTypes.have_postalcodes,
            SearchOutputTypes.have_house_numbers,
            SearchOutputTypes.area_address,
            SearchOutputTypes.point,
            SearchOutputTypes.formatted_address,
            SearchOutputTypes.rank,
        ]
        _format["R"] = [
            SearchOutputTypes.primary_name,
            SearchOutputTypes.area_address,
            SearchOutputTypes.formatted_address,
            SearchOutputTypes.distance,
            SearchOutputTypes.point,
            SearchOutputTypes.postal_code,
            SearchOutputTypes.rank,
            SearchOutputTypes.have_house_numbers,
        ]
        _format["X"] = [
            SearchOutputTypes.primary_name,
            SearchOutputTypes.area_address,
            SearchOutputTypes.formatted_address,
            SearchOutputTypes.distance,
            SearchOutputTypes.rank,
        ]
        _format["E"] = _format["R"]
        _format["A"] = [
            SearchOutputTypes.primary_name,
            SearchOutputTypes.area_id,
            SearchOutputTypes.distance,
            SearchOutputTypes.point,
            SearchOutputTypes.area_address,
            SearchOutputTypes.formatted_address,
            SearchOutputTypes.formatted_area_address,
            SearchOutputTypes.formatted_address_without_postalcode_and_country,
            SearchOutputTypes.rank,
        ]
        _format["S"] = [
            SearchOutputTypes.primary_name,
            SearchOutputTypes.area_id,
            SearchOutputTypes.distance,
            SearchOutputTypes.point,
            SearchOutputTypes.area_address,
            SearchOutputTypes.formatted_area_address,
            SearchOutputTypes.rank,
        ]
        _format["L"] = [
            SearchOutputTypes.primary_name,
            SearchOutputTypes.area_id,
            SearchOutputTypes.distance,
            SearchOutputTypes.point,
            SearchOutputTypes.area_address,
            SearchOutputTypes.formatted_address,
            SearchOutputTypes.formatted_area_address,
            SearchOutputTypes.rank,
        ]
        _format["P"] = [
            SearchOutputTypes.primary_name,
            SearchOutputTypes.area_address,
            SearchOutputTypes.category_id,
            SearchOutputTypes.point,
            SearchOutputTypes.formatted_address,
            SearchOutputTypes.distance,
            SearchOutputTypes.rank,
            SearchOutputTypes.details,
        ]
        _format["H"] = [
            SearchOutputTypes.primary_name,
            SearchOutputTypes.distance,
            SearchOutputTypes.point,
            SearchOutputTypes.entry_point,
            SearchOutputTypes.area_address,
            SearchOutputTypes.formatted_address,
            SearchOutputTypes.formatted_address_without_postalcode_and_country,
            SearchOutputTypes.formatted_street,
            SearchOutputTypes.formatted_area_address,
            SearchOutputTypes.formatted_area_address_postalcode,
            SearchOutputTypes.match_type,
            SearchOutputTypes.rank,
        ]
        _format["Z"] = [
            SearchOutputTypes.distance,
            SearchOutputTypes.primary_name,
            SearchOutputTypes.area_address,
            SearchOutputTypes.point,
            SearchOutputTypes.postal_name,
            SearchOutputTypes.formatted_area_address,
            SearchOutputTypes.formatted_address,
            SearchOutputTypes.rank,
        ]
        _format["W"] = [
            SearchOutputTypes.distance,
            SearchOutputTypes.primary_name,
            SearchOutputTypes.point,
            SearchOutputTypes.area_address,
            SearchOutputTypes.formatted_address,
            SearchOutputTypes.rank,
        ]
        return _format

    def _default_search_config(self):
        """Load default Search configurations"""

        # set output format
        _format = self._default_output_format()
        if self._api.set_output_format(_format).status:
            self._output_format = _format
        else:
            raise Exception("Error setting output format")

        self.full_search_nearby_search = True
        self.locale = "en"

    @property
    def position(self) -> str:
        return self._position

    @position.setter
    def position(self, a_position: Point2D):
        if a_position == self._position:
            return
        result = self._api.update_position(a_position)
        if not result.status:
            log_api_error(result)
            return
        self._position = a_position

    @property
    def locale(self) -> str:
        return self._locale

    @locale.setter
    def locale(self, a_locale: str):
        if a_locale == self._locale:
            return
        result = self._api.set_locale(a_locale)
        if not result.status:
            raise Exception("locale -> {}".format(result.core_status))
            log_api_error(result)
            return
        self._locale = a_locale

    @property
    def full_search_nearby_search(self) -> bool:
        return self._properties["full_search_nearby_search"]

    @full_search_nearby_search.setter
    def full_search_nearby_search(self, a_value: bool):
        self._property_setter_bool("full_search_nearby_search", a_value)

    def stop(self) -> list:
        """Cancel current search operation

        Raise Exception in case of error
        """
        api_result = self._api.stop()
        if not api_result.status:
            log_api_error(api_result)
            raise Exception(api_result.core_status)

    def get(self, a_ids: list) -> list:
        """Executes a Get Api call

        Args:
            a_ids (list): list of ids to be requestd

        Returns:
            [list]: returns the list of results

        Raise Exception in case of error
        """
        basesearch = BaseSearch(self._output_format)
        api_result = self._api.get(
            a_ids, basesearch.status_message, basesearch.data_message
        )
        if not api_result.status:
            log_api_error(api_result)
            raise Exception(api_result.core_status)

        return basesearch.results

    def start_full_search(
        self,
        a_search_string: str,
        a_search_filter: SearchFilters,
        a_update_handler: SearchUpdateHandler = None,
    ) -> list:
        """Executes a Full Search

        Args:
            a_search_string (str): string to search
            a_search_filter (str): a valid string with filter definition
            a_update_handler ([type], optional): [description]. SearchUpdateHandler to receive search update

        Returns:
            [list]: ordered results

        Raise Exception in case of error
        """

        basesearch = BaseSearch(self._output_format)
        if a_update_handler:
            basesearch.add_listener(a_update_handler)
        api_result = self._api.start_full_search(
            a_search_string,
            a_search_filter,
            basesearch.status_message,
            basesearch.data_message,
        )
        if not api_result.status:
            log_api_error(api_result)
            raise Exception(api_result.core_status)

        return basesearch.results

    def start_search(
        self,
        a_search_string: str,
        a_search_filter: SearchFilters,
        a_update_handler: SearchUpdateHandler = None,
    ) -> list:
        """Executes a Search

        Args:
            a_search_string (str): string to search
            a_search_filter (SearchFilters): a valid string with filter definition
            a_update_handler ([type], optional): [description]. SearchUpdateHandler to receive search update

        Returns:
            [list]: ordered results

        Raise Exception in case of error
        """
        basesearch = BaseSearch(self._output_format)
        if a_update_handler:
            basesearch.add_listener(a_update_handler)

        api_result = self._api.start_search(
            a_search_string,
            a_search_filter,
            basesearch.status_message,
            basesearch.data_message,
        )
        if not api_result.status:
            log_api_error(api_result)
            raise Exception(api_result.core_status)

        return basesearch.results

    def geocode(
        self,
        a_search_dict: dict,
        a_record_type: RecordTypeFilter,
        a_update_handler: SearchUpdateHandler = None,
        a_try_partial_words: bool = True
    ) -> list:
        """Executes a Geocode operation

        Args:
            a_search_dict (dict): dict with search parameters
            a_record_type (RecordTypeFilter): RecordTypeFilter - definition of result types
            a_update_handler ([type], optional): SearchUpdateHandler to receive search update
            a_try_partial_words (bool): Should try partial words match? Default: True

        Returns:
            [list]: ordered results

        Raise Exception in case of error

        Example: a_search_dict = {'state_name': a_state, 'city_name': a_city}
                 a_record_type = RecordTypeFilter
        """
        basesearch = BaseSearch(self._output_format)
        if a_update_handler:
            basesearch.add_listener(a_update_handler)

        api_result = self._api.geocode(
            a_search_dict,
            a_record_type,
            basesearch.status_message,
            basesearch.data_message,
            a_try_partial_words
        )
        if not api_result.status:
            log_api_error(api_result)
            raise Exception(api_result.core_status)

        return basesearch.results

    def reverse_geocode(
        self,
        a_location: Point2D,
        a_record_type: RecordTypeFilter,
        a_update_handler: SearchUpdateHandler = None,
    ) -> list:
        """Executes a Reverse Geocode operation

        Args:
            a_location (Point2D): location for which you wish to obtain the closest, human-readable address.
            a_record_type (RecordTypeFilter): RecordTypeFilter - definition of result types
            a_update_handler ([type], optional): SearchUpdateHandler to receive search update

        Returns:
            [list]: ordered results

        Raise Exception in case of error
        """
        basesearch = BaseSearch(self._output_format)
        if a_update_handler:
            basesearch.add_listener(a_update_handler)

        api_result = self._api.reverse_geocode(
            a_location,
            a_record_type,
            basesearch.status_message,
            basesearch.data_message,
        )
        if not api_result.status:
            log_api_error(api_result)
            raise Exception(api_result.core_status)

        return basesearch.results

    def start_nearby_search(
        self,
        a_search_string: str,
        a_search_filter: SearchFilters,
        a_update_handler: SearchUpdateHandler = None,
    ) -> list:
        """Executes a Search

        Args:
            a_search_string (str): string to search
            a_search_filter (SearchFilters): a valid string with filter definition
            a_update_handler ([type], optional): [description]. SearchUpdateHandler to receive search update

        Returns:
            [list]: ordered results

        Raise Exception in case of error
        """
        basesearch = BaseSearch(self._output_format)
        if a_update_handler:
            basesearch.add_listener(a_update_handler)

        api_result = self._api.start_nearby_search(
            a_search_string,
            a_search_filter,
            basesearch.status_message,
            basesearch.data_message,
        )
        if not api_result.status:
            log_api_error(api_result)
            raise Exception(api_result.core_status)

        return basesearch.results

    def get_poi_details(
        self,
        a_poi_id: str,
        a_update_handler: SearchUpdateHandler = None,
    ) -> list:
        """Returns details from a POI

        Args:
            a_poi_id (str): poi_id for return
            a_update_handler ([type], optional): [description]. SearchUpdateHandler to receive search update

        Returns:
            [list]: poi_details

        Raise Exception in case of error
        """
        basesearch = BaseSearch(self._output_format)
        if a_update_handler:
            basesearch.add_listener(a_update_handler)

        api_result = self._api.get_poi_details(
            a_poi_id,
            basesearch.status_message,
            basesearch.data_message,
        )
        if not api_result.status:
            log_api_error(api_result)
            raise Exception(api_result.core_status)

        return api_result.request.data

    def get_poi_categories_resources(self, a_force_update=False) -> PoiCategories:
        """Loads poi categories from resources, or cache if available

        Args:
            a_force_update (bool, optional): do not use cache (if available) update poi categories

        Returns:
            PoiCategories: Load por categories from resources
        """

        # use and return cache
        if (self.locale in self._poi_categories
            and self._poi_categories[self.locale] is not None
            and not a_force_update
        ):
            return self._poi_categories[self.locale]

        poi_cat = PoiCategories(locale=self.locale)

        api_result = self._api.get_poi_categories_resources(
            a_type="ALL_RESOURCES",
            a_visibility="ALL",
            a_data_cb=poi_cat.process_data_response,
            a_status_cb=poi_cat.process_status_request,
        )

        if not api_result.status:
            log_api_error(api_result)
            raise Exception(api_result.core_status)

        # save new data into cache
        self._poi_categories_lock.acquire()
        self._poi_categories[self.locale] = poi_cat
        self._poi_categories_lock.release()
        return self._poi_categories[self.locale]