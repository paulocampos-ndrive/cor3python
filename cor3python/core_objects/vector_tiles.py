from threading import Lock
from typing import Optional

from cor3python.core_objects.base import Base
from cor3python.messaging.base_dispatcher import BaseDispatcher
from cor3python.utils.thread_manager import thread_manager
from cor3python.core_objects.api.api_status import ApiStatus
from cor3python.utils.log import log_api_error
from cor3python.core_objects.api.vector_tiles import APIVectorTiles
from cor3python.core_objects.tileset import TileSet
from cor3python.messaging.status_code import Cor3Status


class VectorTiles(Base):
    """
    CORE VectorTiles Instance
    """

    def __init__(self, a_messaging: BaseDispatcher, a_thread=thread_manager.get_thread("VECTOR_TILES")):
        super().__init__()

        self._messaging = a_messaging

        # Create API Level object
        self._api = APIVectorTiles(a_messaging, a_thread)

    def create_tileset(self, a_name: str, a_type: str, a_scheme: str, a_pipeline_count: int, a_folder: str = None) -> TileSet:
        """Creates a new Tileset instance

        Returns:
            TileSet: TileSet instance or None on error
        """

        api_result = self._api.create_tileset(a_name=a_name, a_type=a_type, a_scheme=a_scheme, a_pipeline_count=a_pipeline_count, a_folder=a_folder)
          
        if not api_result.status:
            log_api_error(api_result)
            return None
        
        return TileSet(a_name=a_name, a_type=a_type, a_scheme=a_scheme, a_pipeline_count=a_pipeline_count, a_folder=a_folder, a_messaging=self._messaging)
