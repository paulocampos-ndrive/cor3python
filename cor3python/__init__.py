import logging
_format = '%(asctime)s | %(levelname)s | Thread:%(threadName)s | %(message)s'
logging.basicConfig(format=_format)