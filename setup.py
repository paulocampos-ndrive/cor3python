import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="cor3python",
    version="0.0.1",
    author="Nuno Martins",
    author_email="nuno.martins@ndrive.com",
    description="A python module for cor3 integration",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/ndrive_devs/cor3-python/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
