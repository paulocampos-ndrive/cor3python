import unittest
from cor3python.messaging.core_response import CoreResponse


class CoreResponseTests(unittest.TestCase):
    def test_dict_simple(self):
        message = r'44,D,{master_estimated_time:"20856.919"}'
        response = CoreResponse()
        response.load_raw(message)
        data = response.parse()
        expected = dict()
        expected['master_estimated_time'] = "20856.919"
        self.assertEqual(data, expected)
    
    def test_parse_rb(self):
        message = r'1,D,{id:I4033281R1M,name:"ExitMotorwayRight",street_name:"Newark-Jersey City Tpke",street_names:["Newark-Jersey City Tpke","CR-508 E"],distance:5666.97656,time:273.369628,traffic_delay:0.00000000,traffic_distance:0.00000000,traffic_severity:65535,motorway:yes,toll:no,ferry:no,exit_number:"17A",towards:"Jersey City",signposts_active:[O45I16797982L],signposts_inactive:[O45I16797857N]}'
        response = CoreResponse()
        response.load_raw(message)
        data = response.parse()
        return True

    def test_dict_complex(self):
        message = r'51,S,kErrNone {itinerary_routes:[],total_distance:1073611.87,total_time:38766.2109,"total_delay_time":0.00000000,num_alternatives:0,num_routes:1,itinerary_name:"I-20 E, I-95 N, I-295 N",tbt:[]}'
        response = CoreResponse()
        response.load_raw(message)
        data = response.parse()
        expected = dict()
        expected['itinerary_routes'] = []
        expected['total_distance'] = 1073611.87
        expected['total_time'] = 38766.2109
        expected['total_delay_time'] = 0.00000000
        expected['num_alternatives'] = 0
        expected['num_routes'] = 1
        expected['itinerary_name'] = "I-20 E, I-95 N, I-295 N"
        expected['tbt'] = []
        self.assertEqual(data, expected)

    def test_list_1(self):
        message = r'51,S,kErrNone [1,2,3,"A","B",C]'
        response = CoreResponse()
        response.load_raw(message)
        data = response.parse()
        expected = [1,2,3,"A", "B", "C"]
        self.assertEqual(data, expected)

    def test_identifier1(self):
        message = r'51,S,kErrNone teste1'
        response = CoreResponse()
        response.load_raw(message)
        data = response.parse()
        expected = ["teste1"]
        self.assertEqual(data, expected)        

    def test_identifier2(self):
        message = r'51,S,kErrNone [teste1,teste2]'
        response = CoreResponse()
        response.load_raw(message)
        data = response.parse()
        expected = ["teste1", "teste2"]
        self.assertEqual(data, expected)

    def test_string_1(self):
        message = r'51,S,kErrNone ["teste1","teste2"]'
        response = CoreResponse()
        response.load_raw(message)
        data = response.parse()
        expected = ["teste1", "teste2"]
        self.assertEqual(data, expected)

    def test_string_2(self):
        message = r'51,S,kErrNone "t\\este\\"'
        response = CoreResponse()
        response.load_raw(message)
        data = response.parse()
        expected = ["t\\este\\"]
        self.assertEqual(data, expected)

    def test_string_3(self):
        message = r'6,D,P,Add,P123I540959774277729,"Wall Street English",["Avenidas Novas","Lisboa","Lisboa","Portugal Continental","Portugal"],T241,[C,-9.1477384,38.7350692],"Rua Filipe Folque, 48\\n1050-114 Avenidas Novas, Lisboa, Portugal Continental, Portugal",499.718017,5763779655203029491'
        response = CoreResponse()
        response.load_raw(message)
        data = response.parse()
        expected = ['P','Add','P123I540959774277729', "Wall Street English", ["Avenidas Novas","Lisboa","Lisboa","Portugal Continental","Portugal"],"T241",["C",-9.1477384,38.7350692],"Rua Filipe Folque, 48\\n1050-114 Avenidas Novas, Lisboa, Portugal Continental, Portugal",499.718017,5763779655203029491]
        self.assertEqual(data, expected)

    def test_dict_1(self):
        message = r'51,S,kErrNone {xpto:""}'
        response = CoreResponse()
        response.load_raw(message)
        data = response.parse()
        expected = {'xpto': ""}
        self.assertEqual(data, expected)

    def test_dict_2(self):
        message = r'6,D,c1I2C1,{search_fields:[Name,Desc]}'
        response = CoreResponse()
        response.load_raw(message)
        data = response.parse()
        expected = ['c1I2C1',{'search_fields':['Name','Desc']}]
        self.assertEqual(data, expected)

    def test_escape_1(self):
        message = r'51,S,kErrNone [1,2,3,"\"A\"","B",C]'
        response = CoreResponse()
        response.load_raw(message)
        data = response.parse()
        expected = [1,2,3,"\"A\"", "B", "C"]
        self.assertEqual(data, expected)

    def test_escape_2(self):
        message = r'51,S,kErrNone "123\\456\\n89\\"'
        response = CoreResponse()
        response.load_raw(message)
        data = response.parse()
        expected = ["123\\456\\n89\\"]
        self.assertEqual(data, expected)

if __name__ == '__main__':
    unittest.main()
    