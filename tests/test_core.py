from cor3python.core_objects.common.types.point_2d import Point2D
import unittest
import os

from cor3python.core_objects.common.search.update_handler import SearchUpdateEvent, SearchUpdateHandler
from cor3python.core_objects.common.search.search_filters import SearchFilters, RecordTypeFilter
from cor3python.utils.thread_manager import thread_manager


class CoreTests(unittest.TestCase):
    PRODUCT_FILE = "./tests/data/maps/RH-PRI-MAP.map"
    PRODUCTS_FOLDER = "./tests/data/maps/"
    RESOURCES_FOLDER = "./tests/data/resources/"
    LIB_COR3 = '~/work/cor3/builds/linux/bin/shared/libsharedcor3.dylib'

    def __init__(self, methodName: str) -> None:
        super().__init__(methodName=methodName)
        self.notification_count = 0

    @classmethod
    def setUpClass(cls):
        from cor3python.core import Core
        cls.core = Core(a_verbose=True)
        cls.core.start_native(CoreTests.LIB_COR3, Core.Cor3InitParams.RenderingEngine.kNone)
        cls.core.licensing.add_product_folder(CoreTests.PRODUCTS_FOLDER)
        cls.core.licensing.add_product_folder(CoreTests.RESOURCES_FOLDER)

    @classmethod
    def tearDownClass(cls):
        cls.core.stop()

    def test_licensing_get_product_info(self):
        filename = os.path.abspath(os.path.expanduser(CoreTests.PRODUCT_FILE))
        info = self.core.licensing.get_product_info(filename)
        self.assertEqual(info.filename, filename)

    def test_licensing_add_remove_products(self):
        product = self.core.licensing.get_product_info(CoreTests.PRODUCT_FILE)
        ret1 = self.core.licensing.remove_products([product])
        ret2 = self.core.licensing.add_products([product])
        self.assertTrue(ret1[0])
        self.assertTrue(ret2[0])

    def test_search_get(self):
        result = self.core.search.get(['C630'])
        self.assertEqual(result[0].id, 'C630')

    def test_search_start_full_search(self):
        results = self.core.search.start_full_search('Puerto Rico', SearchFilters(a_record_types=[('CRASHZLW', 40),('P',40)]))
        self.assertEqual(results[0].id, 'C630')   

    def test_search_start_full_search_error(self):
        try:
            self.core.search.start_full_search('Puerto Rico',  SearchFilters(a_record_types=[('', 40),('P',40)]))
        except Exception:
            return
        self.assertIsNone(None) 

    def test_search_start_full_search_notifications(self):
        self.notification_count = 0
        update_handler = SearchUpdateHandler(thread_manager.get_thread("SEARCH_TEST_NOTIFICATION"), self._search_notification)
        results = self.core.search.start_full_search('Puerto Rico', SearchFilters(a_record_types=[('CRASHZLW', 40),('P',40)]), update_handler)
        self.assertEqual(self.notification_count, len(results))   
    
    def _search_notification(self, a_event:SearchUpdateEvent):
        if a_event.type == SearchUpdateEvent.UpdateType.ADD:
            self.notification_count += 1
        if a_event.type == SearchUpdateEvent.UpdateType.DELETE:
            self.notification_count -= 1            
        if a_event.type == SearchUpdateEvent.UpdateType.UPDATE:
            pass

    def test_search_start_search(self):
        results = self.core.search.start_search('Puerto Rico', SearchFilters(a_record_types=[('CRAHZLW', 40),('P',40)]))
        self.assertEqual(results[0].id, 'C630')        

    def test_search_start_search_with_filter(self):
        results = self.core.search.start_search('Puerto Rico', SearchFilters(a_record_types=[('C', 40)], a_ids=['C630']))
        self.assertEqual(results[0].id, 'C630')          

    def test_geocode(self):
        results = self.core.search.geocode({'country_name': 'Puerto Rico'}, RecordTypeFilter(['A','L','C']))
        self.assertEqual(results[0].id, 'C630')   

    def test_search_executor_sync(self):
        from cor3python.executors.search_executor import SearchExecutor, SearchTask

        executor = SearchExecutor(self.core, a_num_workers=2)
    
        task1 = SearchTask(SearchTask.TaskType.Geocode, [{'country_name': 'Puerto Rico'}, RecordTypeFilter(['A','L','C'])])
        task2 = SearchTask(SearchTask.TaskType.Start, ['Puerto Rico', SearchFilters(a_record_types=[('CRAHZLW', 40),('P',40)])])
        task3 = SearchTask(SearchTask.TaskType.StartFullSearch, ['Puerto Rico', SearchFilters(a_record_types=[('CRASHZLW', 40),('P',40)])])

        executor.add_task(task1)
        executor.add_task(task2)
        executor.add_task(task3)
        executor.run_sync()

        self.assertEqual(task1.results[0].id, 'C630')   
        self.assertEqual(task2.results[0].id, 'C630')  
        self.assertEqual(task3.results[0].id, 'C630')  

    def test_search_executor_async(self):
        from cor3python.executors.search_executor import SearchExecutor, SearchTask

        executor = SearchExecutor(self.core, a_num_workers=2)
    
        task1 = SearchTask(SearchTask.TaskType.Geocode, [{'country_name': 'Puerto Rico'}, RecordTypeFilter(['A','L','C'])], a_language_code='es')
        task2 = SearchTask(SearchTask.TaskType.Start, ['Puerto Rico', SearchFilters(a_record_types=[('CRAHZLW', 40),('P',40)])])
        task3 = SearchTask(SearchTask.TaskType.StartFullSearch, ['Puerto Rico', SearchFilters(a_record_types=[('CRASHZLW', 40),('P',40)])])
        task4 = SearchTask(SearchTask.TaskType.StartFullSearch, ['Calle', SearchFilters(a_record_types=[('CRASHZLW', 40),('P',40)])])
        task5 = SearchTask(SearchTask.TaskType.StartFullSearch, ['a', SearchFilters(a_record_types=[('CRASHZLW', 40),('P',40)])])
        task6 = SearchTask(SearchTask.TaskType.StartFullSearch, ['b', SearchFilters(a_record_types=[('CRASHZLW', 40),('P',40)])])
        task7 = SearchTask(SearchTask.TaskType.StartFullSearch, ['c', SearchFilters(a_record_types=[('CRASHZLW', 40),('P',40)])])
        task8 = SearchTask(SearchTask.TaskType.StartFullSearch, ['d', SearchFilters(a_record_types=[('CRASHZLW', 40),('P',40)])])
        task9 = SearchTask(SearchTask.TaskType.StartFullSearch, ['e', SearchFilters(a_record_types=[('CRASHZLW', 40),('P',40)])])

        executor.start_async()

        executor.add_task(task1)
        executor.add_task(task2)
        executor.add_task(task3)

        task1.wait_for_execution()
        self.assertEqual(task1.results[0].id, 'C630')   
        task2.wait_for_execution()
        self.assertEqual(task2.results[0].id, 'C630')  
        task3.wait_for_execution()
        self.assertEqual(task3.results[0].id, 'C630')  

        executor.add_task(task4)
        executor.add_task(task5)
        executor.add_task(task6)
        executor.add_task(task7)
        executor.add_task(task8)
        executor.add_task(task9)
        
        executor.stop_async()

    def test_search_start_nearby_search_with_filter(self):
        results = self.core.search.start_nearby_search('Puerto Rico', SearchFilters(a_record_types=[('C', 40)], a_ids=['C630']))
        self.assertEqual(results[0].id, 'C630')     

    def test_search_reverse_geocode(self):
        # TODO:
        self.assertFalse(True)

    def test_search_get_poi_categories(self):
        poi_categories = self.core.search.get_poi_categories_resources()
        restaurants = poi_categories.find('T2')
        self.assertEqual(restaurants.name, "Restaurants")

    def test_create_profile(self):
        from cor3python.core_objects.common.routing.profile import Profile

        profile = self.core.routing.create_profile(Profile())
        self.assertIsNotNone(profile)

    def test_create_profile_duplicated(self):
        from cor3python.core_objects.common.routing.profile import Profile

        profile1 = self.core.routing.create_profile(Profile())
        profile2 = self.core.routing.create_profile(Profile())
        self.assertEqual(profile1, profile2)

    def test_create_itinerary(self):
        iti_1 = self.core.routing.create_itinerary("test_create_itinerary")
        iti_1.destroy()
        self.assertIsNotNone(iti_1)       

    def test_clear_itinerary(self):
        iti_1 = self.core.routing.create_itinerary("test_clear_itinerary")
        result = iti_1.clear()
        iti_1.destroy()
        self.assertTrue(result)           

    def test_calc_itinerary(self):
        from cor3python.core_objects.common.routing.route_calculation import RouteCalculation
        from cor3python.core_objects.common.routing.waypoint import WayPoint
        from cor3python.core_objects.common.routing.profile import Profile

        route = RouteCalculation()
        route.profile = self.core.routing.create_profile(Profile())
        iti_1 = self.core.routing.create_itinerary("test_calc_itinerary")
        iti_1.add_waypoint(WayPoint(a_point=Point2D(a_lat=18.406206, a_lng=-66.077858)))
        iti_1.add_waypoint(WayPoint(a_point=Point2D(a_lat=18.381586, a_lng=-66.067740)))

        result_bool = iti_1.calculate(route)
        self.assertTrue(result_bool)      
        self.assertEqual(iti_1.calculation_result.num_routes, 1)

    def test_calc_itinerary_progress(self):
        from cor3python.core_objects.common.routing.route_calculation import RouteCalculation
        from cor3python.core_objects.common.routing.waypoint import WayPoint
        from cor3python.core_objects.common.routing.profile import Profile
        from cor3python.utils.thread_manager import thread_manager
        self.cur_progress = 0

        def cb_progress(a_progress):
            self.cur_progress = a_progress

        thread = thread_manager.get_thread()

        route = RouteCalculation()
        
        route.progress_enable = True
        route.profile = self.core.routing.create_profile(Profile())
        iti_1 = self.core.routing.create_itinerary("test_calc_itinerary_progress")
        iti_1.register_progress_listener(thread, cb_progress)
        iti_1.add_waypoint(WayPoint(a_point=Point2D(a_lat=18.406206, a_lng=-66.077858)))
        iti_1.add_waypoint(WayPoint(a_point=Point2D(a_lat=18.381586, a_lng=-66.067740)))

        result_bool = iti_1.calculate(route)

        self.assertTrue(result_bool)      
        self.assertEqual(self.cur_progress, 100)        

    def test_optimal_waypoints_order(self):
        from cor3python.core_objects.common.routing.waypoint import WayPoint
        from cor3python.utils.thread_manager import thread_manager

        iti_1 = self.core.routing.create_itinerary("test_optimal_waypoints_order")
        wp1 = WayPoint(a_point=Point2D(a_lat=18.406206, a_lng=-66.077858))
        wp2 = WayPoint(a_point=Point2D(a_lat=18.381586, a_lng=-66.067740))
        wp3 = WayPoint(a_point=Point2D(a_lat=18.506206, a_lng=-66.077858))
        wp4 = WayPoint(a_point=Point2D(a_lat=18.606206, a_lng=-66.077858))
        wp5 = WayPoint(a_point=Point2D(a_lat=18.706206, a_lng=-66.077858))
        waypoints = [wp1, wp2, wp3, wp4, wp5]

        result = iti_1.get_optimal_waypoints_order(a_waypoints_list=waypoints)

        self.assertTrue(len(result) == len(waypoints))      


if __name__ == '__main__':
    unittest.main()
    