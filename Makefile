
.venv:
	virtualenv .venv -p python3
	( . .venv/bin/activate; \
      pip install -r requirements/dev.txt; \
	  pip install -e . \
    )

dev_environment: .venv
	@echo Done

run_tests:
	.venv/bin/python -m unittest discover -s ./tests/ -p 'test*.py' -v

doxygen:
	cd docs; doxygen

clean:
	rm -rf .venv
	rm -rf build
	rm -rf dist